import React, { useEffect, useState } from "react";
import { Recommend } from "../components/recommend";
import { notificationApi } from "../apis/notificationApi";
import NotificationItem from "../components/notificationItem";
import { NotificationActions } from "../slice/notificationSlice";
import { useDispatch } from "react-redux";

const Feature = [
  { title: "Tất cả", shortcut: "all" },
  { title: "Chưa đọc", shortcut: "unread" },
];

const NotificationLayout = () => {
  const [tab, setTab] = useState("profile");
  const [listNotifi, setListNotifi] = useState([]);
  const [type, setType] = useState("all");

  useEffect(() => {
    const fecthData = async () => {
      await notificationApi
        .getNotifications()
        .then((respone) => {
          // dispatch(NotificationActions.addNotification(respone.payload));
          setListNotifi(respone.payload);
          console.log(respone.payload);
        })
        .catch((err) => {});
    };
    fecthData();
  }, [type]);

  const handleTab = (value) => {
    return value === type
      ? "text-blue-800"
      : "text-gray-600 hover:text-gray-900";
  };
  return (
    <>
      <div>
        <div className="grid grid-cols-3">
          <div className="col-span-3 py-8 pl-48 pr-32">
            <header className="mb-6">
              <p className="text-5xl font-semibold">Thông báo</p>
            </header>
            <div className="content">
              <div className="mb-6 border-b border-gray-200">
                <ul className="flex mb-2 flex-wrap text-sm font-medium text-center">
                  {Feature.map((item, index) => (
                    <li key={index} className="me-2" role="presentation">
                      <button
                        onClick={() =>
                          setType(item.shortcut === "all" ? "all" : "unread")
                        }
                        className={` ${handleTab(
                          item.shortcut
                        )} inline-block py-2.5 px-4 border-0 rounded-t-lg `}
                        type="button"
                      >
                        {item.title}
                      </button>
                    </li>
                  ))}
                </ul>
              </div>
              <div className="space-y-2">
                {listNotifi ? (
                  listNotifi.map((item, index) => (
                    <NotificationItem key={index} item={item} modal={false} />
                  ))
                ) : (
                  <p className="text-sm font-medium pl-4">
                    Không có thông báo nào
                  </p>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const NotificationsPage = () => {
  return (
    <div>
      <Recommend children={<NotificationLayout />} />
    </div>
  );
};

export default NotificationsPage;
