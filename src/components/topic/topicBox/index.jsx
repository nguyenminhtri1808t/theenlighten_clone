import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { publicPostsApi } from "../../../apis/public/postsApi";

export default function TopicBlogList() {
  const [trendings, setTrendings] = useState([]);

  // useEffect(() => {
  //   const fetchBlogTrending = async () => {
  //     const res = await publicPostsApi.getPostsTrending();
  //     setTrendings(res.data);
  //   };

  //   fetchBlogTrending();
  // });

  console.log(trendings);

  return (
    <>
      <section className="py-[30px] border-t-gray-200 border-b-gray-200 border-[1px] border-l-0 border-r-0">
        <div className="xl:px-0">
          <div className="">
            <h5 className="font-roboto font-medium text-sm xl:text-lg">
              Gợi ý chủ đề cho bạn
            </h5>

            <div className="flex justify-start items-center gap-4 my-5 flex-wrap">
              {/* {topics.map((topic: ITopic, index: number) => (
                <TopicItemComponent key={index} topic={topic} />
              ))} */}
              <div className="py-1.5 px-5 rounded-2xl bg-gray-100 hover:bg-gray-200">
                <Link
                  to={"/explore-blogs"}
                  className="font-roboto text-sm text-black"
                >
                  Explore Blogs
                </Link>
              </div>
              <div className="py-1.5 px-5 rounded-2xl bg-gray-100 hover:bg-gray-200">
                <Link
                  to={"/topic/cong-nghe"}
                  className="font-roboto text-sm text-black"
                >
                  Công Nghệ
                </Link>
              </div>
              <div className="py-1.5 px-5 rounded-2xl bg-gray-100 hover:bg-gray-200">
                <Link
                  to={"/topic/java"}
                  className="font-roboto text-sm text-black"
                >
                  Java
                </Link>
              </div>
              <div className="py-1.5 px-5 rounded-2xl bg-gray-100 hover:bg-gray-200">
                <Link
                  to={"/topic/nodejs"}
                  className="font-roboto text-sm text-black"
                >
                  NodeJs
                </Link>
              </div>
              <div className="py-1.5 px-5 rounded-2xl bg-gray-100 hover:bg-gray-200">
                <Link
                  to={"/topic/reactjs"}
                  className="font-roboto text-sm text-black"
                >
                  ReactJs
                </Link>
              </div>
              

              <div className="py-1.5 px-5 rounded-2xl bg-gray-100 hover:bg-gray-200">
                <Link
                  to={"/topic/backend"}
                  className="font-roboto text-sm text-black"
                >
                  Backend
                </Link>
              </div>

              <div className="py-1.5 px-5 rounded-2xl bg-gray-100 hover:bg-gray-200">
                <Link
                  to={"/topic/frontend"}
                  className="font-roboto text-sm text-black"
                >
                  Frontend
                </Link>
              </div>
            </div>

            <Link
              to={"/explore-topic"}
              className="text-sm font-roboto text-lime-700 font-medium"
            >
              Xem thêm topic
            </Link>
          </div>
        </div>
      </section>
    </>
  );
}
