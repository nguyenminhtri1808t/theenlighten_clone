import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { notificationApi } from "../../apis/notificationApi";
import NotificationItem from "../notificationItem";

const NotificationModal = () => {
  // const [tab, setTab] = useState("profile");
  const [listNotifi, setListNotifi] = useState([]);
  const [type, setType] = useState("all");

  useEffect(() => {
    const fecthData = async () => {
      await notificationApi
        .getNotifications()
        .then((respone) => {
          setListNotifi(respone.payload);
          console.log(respone.payload);
        })
        .catch((err) => {});
    };
    fecthData();
  }, [type]);

  const handleHeightModal = () => {
    listNotifi.length > 5 ? "h-notifi" : "h-fit";
  };

  const handleTab = (value) => {
    return value === type
      ? "text-blue-800"
      : "text-gray-600 hover:text-gray-900";
  };

  return (
    <div
      className={`notification-modal w-96 bg-gray-200 rounded-lg shadow-lg p-5 px-2 ${handleHeightModal()} overflow-y-scroll`}
    >
      <p className="text-center text-lg font-medium mb-3 px-3">Thông báo</p>
      <div className="space-x-3 mb-3 px-3">
        <button className={`font-medium ${handleTab("all")}`}>Tất cả</button>
        <button className={`font-medium ${handleTab("unread")}`}>
          Chưa đọc
        </button>
      </div>
      <div className="flex justify-between mb-2 px-3">
        <p>Gần đây</p>
        <Link className="text-blue-700" to={"/notifications"}>
          Xem tất cả
        </Link>
      </div>
      <div className="space-y-1">
        {listNotifi.map((item, index) => (
          <NotificationItem key={index} item={item} modal={true} />
        ))}
      </div>
    </div>
  );
};

export default NotificationModal;
