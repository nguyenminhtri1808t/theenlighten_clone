import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { auth } from "../../firebase";
import { authenticationApi } from "../../apis/public/authenticationApi";
import { useAppDispatch } from "../../hooks/reduxHook";
import { authenticationActions } from "../../slice/authenticationSlice";
import toast from "react-hot-toast";

export default function HeaderNotUser() {
  const [isSticky, setIsSticky] = useState(false);
  const dispatch = useAppDispatch();
  const pathname = window.location.pathname;

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 0) {
        setIsSticky(true);
      } else {
        setIsSticky(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleSignIn = async () => {
    const provider = new GoogleAuthProvider();
    const clientToken = localStorage.getItem("vapidToken");
    await signInWithPopup(auth, provider)
      .then(async (response) => {
        const cleanedToken = response.user.accessToken;
        localStorage.setItem("token", cleanedToken);
        await authenticationApi
          .login(clientToken)
          .then((response) => {
            if (response) toast("Đăng nhập thành công", { icon: "🔶" });
            dispatch(
              authenticationActions.login({
                user: response.payload,
                token: cleanedToken,
                timeToken: new Date().getTime(),
              })
            );
          })
          .catch((error) => {
            console.log(error);
            toast.error("Đăng nhập không thành công");
          });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const headerClasses = isSticky ? "bg-white" : "";

  const isHome = () => {
    return pathname === "/" ? "bg-amber-400" : "";
  };

  return (
    <>
      <header
        className={`font-roboto ${isHome()} z-50  border-b border-b-gray-100 shadow-md md:px-7 ${headerClasses} sticky top-0 z-50 transition ease-in-out delay-0`}
      >
        <div className="container mx-auto py-5 flex justify-between items-center">
          <div className="w-fit overflow-hidden h-12 flex items-center justify-center">
            <Link to={"/"} className=" mr-4">
              <span className="self-center text-2xl text-primary font-sans font-semibold whitespace-nowrap tracking-widest">
                enlighten <span className="">.</span>
              </span>
            </Link>
          </div>
          <div className="">
            <ul className="hidden md:flex md:items-center md:gap-6 md:text-sm xl:flex xl:items-center xl:gap-5 xl:text-black xl:text-sm">
              <li>
                <Link to={"/our-story"}>Câu chuyện của bạn</Link>
              </li>
              <li>
                <Link to={"/member"}>Thành viên</Link>
              </li>
              <li>
                <Link to={"/write"}>Viết bài</Link>
              </li>
              <li>
                <button onClick={handleSignIn}>Đăng nhập</button>
              </li>
              <li>
                <button
                  onClick={handleSignIn}
                  className="px-4 py-3 bg-black rounded-3xl text-white font-medium"
                >
                  Bắt đầu
                </button>
              </li>
            </ul>
            <div className="md:hidden xl:hidden">
              <button
                onClick={handleSignIn}
                className="px-4 py-3 bg-black rounded-3xl text-white font-medium"
              >
                Bắt đầu
              </button>
            </div>
          </div>
        </div>
      </header>
    </>
  );
}
