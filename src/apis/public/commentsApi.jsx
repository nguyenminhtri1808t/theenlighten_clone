import axiosClient from "../axiosClient";

const publicApiUrl = "/public/api/v1";

export const commentsPublicApi = {
  getComments: async (postId) => {
    try {
      const url = `${publicApiUrl}/comments?postId=${postId}`;
      return axiosClient.get(url);
    } catch (error) {
      console.error("Error fetching comments:", error);
      throw error;
    }
  },

};
