import { Link } from "react-router-dom";

export default function Help() {
  return (
    <div className="p-5 border-t-2">
      <ul className="flex flex-wrap gap-4 font-roboto text-sm text-slate-400">
        <li className="hover:text-black">
          <Link to={"/"}>Hỗ trợ</Link>
        </li>

        {/* <li className="hover:text-black">
            <Link to={"/"}>Status</Link>
          </li> */}
        <li className="hover:text-black">
          <Link to={"/"}>Về chúng tôi</Link>
        </li>
        {/* <li className="hover:text-black">
            <Link to={"/"}>Career</Link>
          </li> */}
        <li className="hover:text-black">
          <Link to={"/"}>Bài viết</Link>
        </li>
        <li className="hover:text-black">
          <Link to={"/"}>Thanh khoản</Link>
        </li>
        <li className="hover:text-black">
          <Link to={"/"}>Điều khoản</Link>
        </li>
        {/* <li className="hover:text-black">
            <Link to={"/"}>Text to speech</Link>
          </li> */}
        <li className="hover:text-black">
          <Link to={"/"}>Đội nhóm</Link>
        </li>
      </ul>
    </div>
  );
}
