import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getMessaging, getToken, onMessage } from "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyCPWmuFbzOP_s-0WmF4sOPzqCd7AH1MVKI",
  authDomain: "kaka-5c946.firebaseapp.com",
  projectId: "kaka-5c946",
  storageBucket: "kaka-5c946.appspot.com",
  messagingSenderId: "579392227763",
  appId: "1:579392227763:web:65b1ec29e56b946d2d63c6",
  measurementId: "G-LYVLJWNK7P",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const messaging = getMessaging(app);

// export const getOrRegisterServiceWorker = () => {
//   if ("serviceWorker" in navigator) {
//     return window.navigator.serviceWorker
//       .getRegistration("/firebase-push-notification-scope")
//       .then((serviceWorker) => {
//         if (serviceWorker) return serviceWorker;
//         return window.navigator.serviceWorker.register(
//           "/firebase-messaging-sw.js",
//           {
//             scope: "/firebase-push-notification-scope",
//           }
//         );
//       });
//   }
//   throw new Error("The browser doesn`t support service worker.");
// };

// export const getFirebaseToken = () =>
//   getOrRegisterServiceWorker().then((serviceWorkerRegistration) =>
//     getToken(messaging, {
//       vapidKey:
//         "BCmeefLUCsruv8M7e2MQT5tGiXuMOVWsTb7snCyOkAf5mfMNL_Gh1o89PDk0lTpqXUeuyShCH0BGHX7SCd1EYQo",
//       serviceWorkerRegistration,
//     })
//       .then((currentToken) => {
//         console.log("Client Token: ", currentToken);
//         localStorage.setItem("vapidToken", currentToken);
//       })
//       .catch((err) => {
//         console.log(
//           "An error occurred when requesting to receive the token.",
//           err
//         );
//       })
//   );

// getFirebaseToken();

// export const onForegroundMessage = () =>
//   new Promise((resolve) => onMessage(messaging, (payload) => resolve(payload)));

export const requestPermission = () => {
  console.log("Requesting User Permission......");
  Notification.requestPermission().then((permission) => {
    if (permission === "granted") {
      console.log("Notification User Permission Granted.");
      return getToken(messaging, {
        vapidKey: `BCmeefLUCsruv8M7e2MQT5tGiXuMOVWsTb7snCyOkAf5mfMNL_Gh1o89PDk0lTpqXUeuyShCH0BGHX7SCd1EYQo`,
      })
        .then((currentToken) => {
          if (currentToken) {
            console.log("Client Token: ", currentToken);
            localStorage.setItem("vapidToken", currentToken);
          } else {
            console.log("Failed to generate the app registration token.");
          }
        })
        .catch((err) => {
          console.log(
            "An error occurred when requesting to receive the token.",
            err
          );
        });
    } else {
      console.log("User Permission Denied.");
    }
  });
};

requestPermission();

export const onMessageListener = () =>
  new Promise((resolve) => {
    onMessage(messaging, (payload) => {
      resolve(payload);
    });
  });
