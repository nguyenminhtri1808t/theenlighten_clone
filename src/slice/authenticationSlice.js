import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  token: null,
  user: null,
  vapidToken: null,
  timeToken: null,
};

const authenticationSlice = createSlice({
  name: "authentication",
  initialState,
  reducers: {
    login: (state, action) => {
      console.log(action.payload);
      state.token = action.payload.token;
      state.user = action.payload.user;
      state.timeToken = action.payload.timeToken;
    },
    logout: (state) => {
      state.token = null;
      state.user = null;
      state.timeToken = null;
      localStorage.removeItem("token");
    },
    addVapidToken: (state, action) => {
      state.vapidToken = action.payload;
    },
    updateUser: (state, action) => {
      state.user = action.payload;
    },
  },
});

export const authenticationActions = authenticationSlice.actions;
export const authenticationReducer = authenticationSlice.reducer;
