import axiosClient from "../axiosClient";

export const authenticationApi = {
  login: async (clientToken) => {
    const data = { clientToken };
    return await axiosClient.post("/api/v1/accounts/login", data, {
      headers: {
        "Content-Type": "application/json",
      },
    });
  },
};
