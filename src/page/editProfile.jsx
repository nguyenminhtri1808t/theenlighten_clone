import React, { useState } from "react";
import { Link } from "react-router-dom";
import Profile from "../components/editProflie/profile";
import ProfileModal from "../components/profileModal";
import { useAppSelector } from "../hooks/reduxHook";
import { useDispatch } from "react-redux";
import { ProfileActions } from "../slice/profileSlice";
import UpgradeMember from "../components/upgrade";
import { default as withAuthRedirect } from "../hoc/WrapperAuthentication";

const Feature = [
  {
    title: "Tài khoản",
    shortname: "profile",
    component: <Profile />,
  },
  {
    title: "Xuất bản",
    shortname: "post",
    component: <Profile />,
  },
  {
    title: "Thông báo ",
    shortname: "notification",
    component: <Profile />,
  },
  {
    title: "Thành viên và Thanh toán ",
    shortname: "member",
    component: <UpgradeMember />,
  },
];

const EditProfile = () => {
  const [tab, setTab] = useState("profile");
  const user = useAppSelector((state) => state.authentication.user);
  const profile = useAppSelector((state) => state.profile.modalEdit);
  const dispatch = useDispatch();

  const [tabContent, setTabContent] = useState(Feature[0].component);

  const handleModalProfile = () => {
    dispatch(ProfileActions.handleModalEdit(false));
  };

  const handleTab = (tab, component) => {
    setTab(tab);
    setTabContent(component);
  };

  return (
    <div className="grid grid-cols-3">
      <div className="col-span-2 py-10 pl-48 pr-32">
        <header className="mb-6">
          <p className="text-5xl font-semibold">Thông tin</p>
        </header>
        <div className="content">
          <div className="mb-12 border-b border-gray-200">
            <ul className="flex flex-wrap -mb-px text-sm font-medium text-center">
              {Feature.map((item, index) => (
                <li key={index} className="me-2" role="presentation">
                  <button
                    onClick={() => handleTab(item.shortname, item.component)}
                    className={` ${
                      tab !== item.shortname
                        ? "hover:text-gray-600 text-gray-500 hover:border-gray-900 dark:hover:text-gray-900"
                        : ""
                    } inline-block py-2.5 px-4 border-0 rounded-t-lg `}
                    type="button"
                  >
                    {item.title}
                  </button>
                </li>
              ))}
            </ul>
          </div>
          <div className="mb-12 pl-4">{tabContent}</div>
          <hr className="mb-12" />
          <Link
            href={"/account/following"}
            className="text-sm font-medium text-lime-600"
          >
            2 Người theo dõi
          </Link>
        </div>
      </div>
      <div
        className={` ${
          profile ? "" : "hidden"
        } fixed inset-0 bg-gray-100/75 z-50 flex justify-center items-center`}
      >
        <ProfileModal
          image={user.avatarUrl}
          nickname={user.nickname}
          handleModalProfile={handleModalProfile}
        />
      </div>
    </div>
  );
};

export default withAuthRedirect(EditProfile);
