import axiosClient from "../axiosClient";

const publicApiUrl = "/public/api/v1";

export const publicPostsApi = {
  gelAllPosts: async (page) => {
    try {
      const url = `/public/api/v1/posts?page=${page}`;
      return await axiosClient.get(url);
    } catch (error) {
      console.error("Error fetching posts:", error);
      throw error;
    }
  },

  getPost: (id) => {
    const url = `${publicApiUrl}/posts/${id}`;
    return axiosClient.get(url);
  },

  getPostsByTopic: (topic, page) => {
    const url = `${publicApiUrl}/posts?sort-by=topic&topics=${topic}&page=${page}`;
    return axiosClient.get(url);
  },

  getPostsTrending: async () => {
    try {
      const url = `${publicApiUrl}/posts?sort-by=trending&size=6`;
      return await axiosClient.get(url);
    } catch (error) {
      console.error("Error fetching posts:", error);
      throw error;
    }
  },

  postBlog: async (data) => {
    const param = {
      backgroundUrl: data.backgroundUrl,
      htmlString: "string",
      description: data.description,
      title: data.title,
      topics: data.topics,
      blocks: data.blocks,
    };

    const url = "/api/v1/posts";

    return await axiosClient.post(url, param, {
      headers: {
        "Content-Type": "application/json",
      },
    });
  },
};
