import React from "react";
import { useAppSelector } from "../../hooks/reduxHook";

const NotificationItem = (props) => {
  const { item, modal } = props;
  const user = useAppSelector((state) => state.authentication.user);
  return (
    <div className="py-2 px-3 rounded-lg">
      <div className="relative flex items-center space-x-3">
        <img className="w-16 h-16 rounded-full" src={user.avatarUrl} alt="" />
        <div className="flex-grow px-1">
          <p className={`${modal ? "text-sm" : ""} font-medium `}>
            {item.title}
          </p>
          <p className={`${modal ? "text-xs" : "text-sm"} `}>{item.message}</p>
        </div>
        <div>
          {item && (
            <div className="w-3 h-3 rounded-full bg-green-500 relative right-0 top-1/2 -translate-y-1/2" />
          )}
        </div>
      </div>
    </div>
  );
};

export default NotificationItem;
