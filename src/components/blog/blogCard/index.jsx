import { Suspense } from "react";
import { Link } from "react-router-dom";
import { IconDownVote, IconSaveBlog, IconUpVote } from "../icon";

export default function BlogCardItem({ blog, accountName, accountAvatar }) {
  const date = new Date(blog.dateCreate);

  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;

  const checkVote =
    blog.upvote - blog.downvote == 0
      ? null
      : blog.upvote - blog.downvote < 1
      ? false
      : blog.upvote - blog.downvote > 1
      ? true
      : null;

  return (
    <>
      <div className="py-10 border-b-[1px] md:border-0 md:py-0">
        <div className="relative img__blog cursor-pointer">
          <img
            src={blog.backgroundUrl}
            alt=""
            className="rounded-xl w-full h-[300px] object-cover object-centers"
          />

          <div
            className={`${
              blog.isPaid ? "" : "hidden"
            } absolute top-[-25px] right-[-20px] text-center rounded-xl px-3 py-3 bg-red-500 text-white font-bold text-xs`}
          >
            Premium
          </div>
        </div>
        <Link
          to={`/blogs/${blog.id}`}
          className="author mt-4 flex justify-start items-center gap-3 cursor-pointer"
        >
          <img
            src={accountAvatar}
            className="rounded-full bg-cover max-w-[30px] max-h-[30px] w-[30px] h-[30px] object-cover object-centers"
            alt="Rounded avatar"
          />

          <h5 className="font-roboto text-sm cursor-pointer">{accountName}</h5>
        </Link>
        <div className="title mt-3">
          <Link
            to={`/blogs/${blog.id}`}
            className="font-bold text-xl text-left line-clamp-2 cursor-pointer text-ellipsis"
          >
            {blog.title}
          </Link>
        </div>
        <div className="content mt-2">
          <p className="text-sm text-gray-500 line-clamp-2 text-left text-ellipsis">
            {blog.description}
          </p>
        </div>
        <div className="mt-3">
          <div className="text-xs text-gray-600 flex justify-start items-center gap-3 lg:text-xs xl:text-xs">
            <span>{formattedDate}</span>
            <div className="concat-string"> - </div>
            <div className="time-reading">8 min read</div>
          </div>
        </div>
        <div className="action flex justify-between items-center mt-2">
          <div className="flex items-center gap-5">
            <div className="like flex items-center gap-2">
              <div className="cursor-pointer">
                <IconUpVote />
              </div>

              <div
                className={`value_like text-gray-500 text-sm cursor-pointer ${
                  checkVote == true
                    ? "text-green-500"
                    : checkVote == null
                    ? ""
                    : ""
                }`}
              >
                {blog.upvote}
              </div>
            </div>

            <div className="like flex items-center gap-2">
              <div className="cursor-pointer">
                <IconDownVote />
              </div>

              <div
                className={`value_like text-gray-500 text-sm cursor-pointer ${
                  checkVote == null
                    ? "text-black"
                    : checkVote == false
                    ? "text-red-500"
                    : "text-black"
                }`}
              >
                {blog.downvote}
              </div>
            </div>
          </div>

          <div className="flex items-center gap-5">
            <div className="like flex items-center gap-2">
              <div className="cursor-pointer">
                <IconSaveBlog />
              </div>
              <svg
                className="w-5 h-5 text-gray-500 dark:text-white hover:text-black cursor-pointer"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                id="more"
              >
                <g data-name="Layer 2">
                  <g data-name="more-horizotnal">
                    <circle cx="12" cy="12" r="2"></circle>
                    <circle cx="19" cy="12" r="2"></circle>
                    <circle cx="5" cy="12" r="2"></circle>
                  </g>
                </g>
              </svg>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
