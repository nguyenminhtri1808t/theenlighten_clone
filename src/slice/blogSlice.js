import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { blogApi } from "~/service/apis/blogService";

const blogsSlice = createSlice({
  name: "blogs",
  initialState: {
    blogs: [],
  },
  reducers: {
    fetchBlogs: (state, action) => {
      state.blogs = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchBlogsAsync.fulfilled, (state, action) => {
      state.blogs = action.payload;
    });
  },
});

export const fetchBlogsAsync = createAsyncThunk(
  "blogs/fetchBlogsAsync",
  async () => {
    const response = await blogApi.getAll();
    const data = response;
    return data;
  }
);
export const { fetchBlogs } = blogsSlice.actions;
export default blogsSlice.reducer;
