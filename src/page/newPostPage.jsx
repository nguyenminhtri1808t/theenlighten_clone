import React, { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import ConfirmModal from "../components/confirmModal";
import NavbarWrite from "../components/navbarWrite";
import TextEditor from "../components/textEditor";
import { default as withAuthRedirect } from "../hoc/WrapperAuthentication";

const DATA_DEFAULT = {
  blocks: [
    {
      id: "l98dyx3yjb",
      type: "header",
      data: {
        text: "",
        level: 1,
      },
    },
  ],
};

const NewPostPage = () => {
  const [save, setsave] = useState(true);
  const [onChange, setOnChange] = useState(false);
  const [dataDefault, setDataDefault] = useState(null);
  const [validate, setValidate] = useState(false);
  const [link, setLink] = useState("");
  const navigate = useNavigate();

  const onSave = useCallback((data) => {
    setsave(data);
    if (!onChange) setOnChange(true);
  }, []);

  useEffect(() => {
    const handleBeforeUnload = (e) => {
      const data = localStorage.getItem("data");
      if (!data) {
        return;
      }
    };
  }, []);

  const handleValidate = useCallback((value, link) => {
    setValidate(value);
    if (value) setLink(link);
  }, []);

  const confirm = useCallback(
    (value) => {
      if (value) {
        navigate(link);
        localStorage.removeItem("data");
      } else {
        setValidate(false);
      }
    },
    [link]
  );

  return (
    <div className="flex justify-center items-center">
      <div className="w-2/3">
        <NavbarWrite
          save={save}
          onChange={onChange}
          handleValidate={handleValidate}
        />
        <div className="a">
          <TextEditor save={onSave} dataDefault={dataDefault} />
        </div>
        <div
          className={` ${
            validate ? "" : "hidden"
          } overflow-y-auto overflow-x-hidden bg-gray-500/50 fixed flex top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-modal md:h-full`}
        >
          <ConfirmModal
            messaging={
              "Bạn muốn hủy bỏ những thay đổi, những thay đổi của bạn sẽ không được lưu lại"
            }
            confirm={confirm}
            type={""}
            handleValidate={handleValidate}
          />
        </div>
      </div>
    </div>
  );
};

export default withAuthRedirect(NewPostPage);
