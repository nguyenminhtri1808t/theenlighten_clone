import React from "react";
import { useDispatch } from "react-redux";
import { ProfileActions } from "../../slice/profileSlice";
import { useNavigate } from "react-router-dom";

const ProfileDetail = (props) => {
  const { title, subtitle, content } = props;
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleActionProfile = () => {
    if (title === "Thông tin cá nhân") {
      dispatch(ProfileActions.handleModalEdit(true));
    }
    if (title === "Nâng cấp tài khoản") {
      navigate("/member");
    }
  };

  return (
    <div
      onClick={handleActionProfile}
      className="flex justify-between text-sm cursor-pointer"
    >
      <div className="">
        <p className="font-medium mb-2">{title}</p>
        <p className="text-gray-600">{subtitle}</p>
      </div>
      <div className="text-end text-gray-500">{content}</div>
    </div>
  );
};

export default ProfileDetail;
