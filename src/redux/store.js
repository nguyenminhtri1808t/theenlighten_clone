import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { authenticationReducer } from "../slice/authenticationSlice";
import { postReducer } from "../slice/postSlice";
import storage from "redux-persist/lib/storage";
import { persistReducer, persistStore } from "redux-persist";
import { profileReducer } from "../slice/profileSlice";
import { notificationReducer } from "../slice/notificationSlice";

const persistConfig = {
  key: "root",
  storage,
};

const rootReducer = combineReducers({
  authentication: authenticationReducer,
  post: postReducer,
  profile: profileReducer,
  notification: notificationReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

// Define the composeEnhancers function
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware())
);

export const persistor = persistStore(store);
