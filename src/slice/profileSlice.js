import { createSlice } from "@reduxjs/toolkit";

const profileSlice = createSlice({
  name: "profile",
  initialState: {
    modalEdit: false,
  },
  reducers: {
    handleModalEdit: (state, action) => {
      state.modalEdit = action.payload;
    },
  },
});

export const ProfileActions = profileSlice.actions;
export const profileReducer = profileSlice.reducer;
