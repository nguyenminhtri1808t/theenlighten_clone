import toast, { Toaster } from "react-hot-toast";
import { Route, Routes, useLocation, useNavigate } from "react-router-dom";
import "./App.css";
import { RootLayout } from "./layout/root";
import HomePage from "./page/homePage";
import NewPostPage from "./page/newPostPage";
import BlogExplorePage from "./page/posts";
import TopicPage from "./page/topic";
import Notification from "./components/notification";
import { AccountPage } from "./page/account";
import { default as EditProfile } from "./page/editProfile";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "./hooks/reduxHook";
import MemberPage from "./page/member";
import DashboardLayout from "./layout/dashboard";
import BrowseArticlesPage from "./page/browseArticles";
import BlogArticlesPage from "./page/blogArticles";
import BlogDetailsPage from "./page/blogDetails";
import { authenticationActions } from "./slice/authenticationSlice";
import NotificationsPage from "./page/notifications";
import NotFoundPage from "./page/errors/404";
import { UserPage } from "./page/user";

const App = () => {
  const location = useLocation();
  const pathname = location.pathname;
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const timeToken = useAppSelector((state) => state.authentication.timeToken);

  useEffect(() => {
    const now = new Date().getTime();
    const time = now - timeToken;
    if (time > 3_600_000 && timeToken) {
      localStorage.removeItem("token");
      dispatch(authenticationActions.logout());
      toast("Vui lòng đăng nhập lại", { icon: "🔶" });
    }
  }, [location]);

  return (
    <>
      <div>
        <Notification />
        <Toaster />
      </div>
      <Routes>
        <Route path="/" element={<RootLayout />}>
          <Route path="" element={<HomePage />} />
          <Route path="topic/:topic" element={<TopicPage />} />
          <Route path="explore-blogs" element={<BlogExplorePage />} />
          <Route path="explore-blogs/:id" element={<BlogDetailsPage />} />
          <Route path="blogs/:id" element={<BlogDetailsPage />} />
          <Route path="account" element={<AccountPage />} />
          <Route path="me/settings" element={<EditProfile />} />
          <Route path="member" element={<MemberPage />} />
          <Route path="notifications" element={<NotificationsPage />} />
          <Route path="profile/:slug" element={<UserPage />} />
        </Route>
        <Route path="new-post" element={<NewPostPage />} />
        <Route path="dashboard" element={<DashboardLayout />}>
          <Route path="browse-articles" element={<BrowseArticlesPage />} />
          <Route
            path="browse-articles/post/:id"
            element={<BlogArticlesPage />}
          />
        </Route>
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </>
  );
};

export default App;
