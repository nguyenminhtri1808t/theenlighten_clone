import { useCallback, useState } from "react";
import { Link } from "react-router-dom";
import { Notification, Write } from "../../assets/svg";
import { useAppSelector } from "../../hooks/reduxHook";
import NotificationModal from "../notificationModal";
import Profile from "../profile";

const HeaderUserComponent = () => {
  const [action, setAction] = useState(false);
  const [notification, setNotification] = useState(false);
  const user = useAppSelector((state) => state.authentication.user);

  return (
    <nav className="shadow-sm bg-white py-2 sticky h-[76px] top-0 z-50">
      <div className="flex flex-wrap justify-between items-center h-full">
        <div className="pl-5 flex justify-start items-center">
          <button
            data-drawer-target="drawer-navigation"
            data-drawer-toggle="drawer-navigation"
            aria-controls="drawer-navigation"
            className="p-2 mr-6 text-gray-600 rounded-lg cursor-pointer md:hidden hover:text-gray-900 hover:bg-gray-100 focus:bg-gray-100 focus:ring-2 focus:ring-gray-100"
          >
            <svg
              aria-hidden="true"
              className="w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h6a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                clipRule="evenodd"
              ></path>
            </svg>
            <svg
              aria-hidden="true"
              className="hidden w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            </svg>
            <span className="sr-only">Toggle sidebar</span>
          </button>

          <div className="w-fit overflow-hidden h-12 flex items-center justify-center ">
            <Link to={"/"} className="mr-4">
              <span className="self-center text-2xl text-primary font-sans font-semibold whitespace-nowrap tracking-widest">
                enlighten <span className="">.</span>
              </span>
            </Link>
          </div>

          <form action="#" method="GET" className="hidden md:block">
            <label htmlFor="topbar-search" className="sr-only">
              Tìm kiếm
            </label>
            <div className="relative md:w-54">
              <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                <svg
                  className="w-5 h-5 text-gray-500"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                  />
                </svg>
              </div>
              <input
                type="text"
                name="email"
                id="topbar-search"
                className=" border border-gray-300 text-gray-900 text-sm rounded-full focus:ring-gray-500 focus:border-gray-500 block w-full pl-10 p-2.5"
                placeholder="Tìm kiếm"
              />
            </div>
          </form>
        </div>

        <div className="hidden md:flex pr-5 items-center justify-end lg:order-2">
          {user.isAdmin ? (
            <>
              <Link
                to={"/dashboard/browse-articles"}
                className="px-3 py-2 rounded-lg cursor-pointer bg-gray-100 mr-5 text-gray-500 hover:bg-blue-500 hover:text-white duration-200 ease-linear"
              >
                Dashboard
              </Link>
            </>
          ) : (
            ""
          )}
          <Link
            to={"/new-post"}
            className="p-2 mr-4 bg-gray-100 text-gray-500 rounded-full hover:text-gray-900 hover:bg-gray-200 focus:ring-4 focus:ring-gray-300"
          >
            <span className="sr-only">Viết bài</span>
            <Write />
          </Link>
          <div className="relative">
            <button
              onClick={() => setNotification(!notification)}
              type="button"
              className="p-2 mr-1 bg-gray-100 text-gray-500 rounded-full hover:text-gray-900 hover:bg-gray-200  focus:ring-4 focus:ring-gray-300"
            >
              <Notification />
            </button>
            <div
              className={`${
                notification ? "" : "hidden"
              } absolute z-50 pt-6 -translate-x-1/2`}
            >
              <NotificationModal />
            </div>
          </div>
          <button
            onClick={() => setAction(!action)}
            type="button"
            className="relative flex mx-3 text-sm  rounded-full md:mr-0 focus:ring-0 "
          >
            <div className="flex flex-col items-end px-3">
              <span className=" font-medium">{user.nickname}</span>
              <span>Chào mừng bạn!</span>
            </div>
            <img
              className="w-10 h-10 rounded-full"
              src={user.avatarUrl}
              alt="user photo"
              width={100}
              height={100}
            />
            <div
              className={`${
                action ? "" : "hidden"
              } absolute left-0 -translate-x-1/3 bottom-0 translate-y-full pt-2 `}
            >
              <Profile />
            </div>
          </button>
        </div>
      </div>
    </nav>
  );
};

export default HeaderUserComponent;
