import { Link } from "react-router-dom";

export default function BlogVerticalItem({ blog, creator }) {
  const date = new Date(blog.dateCreate);

  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;

  const checkVote =
    blog.upvote - blog.downvote > 1
      ? true
      : blog.upvote - blog.downvote < 1
      ? false
      : null;
  return (
    <>
      <div className="grid grid-cols-3 gap-3 xl:gap-12 items-center w-full">
        <div className="col-span-2">
          <div className="flex justify-start items-center gap-3">
            <Link to={`/blogs/${blog.id}`}>
              <img
                src={creator.avatarUrl}
                className="rounded-full w-[30px] h-[30px] object-cover object-center"
              />
            </Link>

            <h5 className="text-xs xl:text-sm font-roboto">
              <Link to={"/account"}>{creator.nickname}</Link>
            </h5>
          </div>

          <div className="py-3 w-full">
            <h5 className="font-roboto text-base font-bold line-clamp-2 text-ellipsis xl:text-xl hover:text-amber-400">
              <Link to={`/blogs/${blog.id}`}>{blog.title}</Link>
            </h5>
            <p className="hidden font-roboto md:line-clamp-2 md:text-ellipsis lg:line-clamp-2 lg:text-ellipsis font-normal xl:text-sm xl:line-clamp-2 xl:text-ellipsis 2xl:line-clamp-2 2xl:text-ellipsis text-gray-500">
              {blog.description}
            </p>
          </div>

          <div className="">
            <div className="text-xs flex justify-start items-center gap-3 lg:text-xs xl:text-xs">
              <span>{formattedDate}</span>
              <div className="concat-string"> - </div>
              <div className="time-reading">5 phút đọc</div>
            </div>
          </div>
        </div>
        <div className="col-span-1 max-h-full relative">
          <Link to={`/blogs/${blog.id}`}>
            <img
              src={blog.backgroundUrl}
              className="w-full h-[200px] bg-clip-content bg-origin-content bg-left bg-cover rounded-xl object-cover object-centers"
              alt=""
            />
          </Link>

          <div
            className={`${
              blog.isPaid ? "" : "hidden"
            } absolute top-[-25px] right-[-20px] text-center rounded-xl px-3 py-3 bg-red-500 text-white font-bold text-xs`}
          >
            Premium
          </div>
        </div>
      </div>
    </>
  );
}
