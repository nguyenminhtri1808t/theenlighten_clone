import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { publicPostsApi } from "../apis/public/postsApi";
import { postInspectApi } from "../apis/private/postInspectApi";
import ActionBlogComponent from "../components/action";
import TopicForBlog from "../components/topic/topicForBlog";
import TextEditorOutput from "../components/textEditorOutput";
import toast from "react-hot-toast";
import SkeletonBlogDetails from "../skeleton/pageBlogDetails";

export default function BlogArticlesPage() {
  const { id } = useParams();
  const [status, setStatus] = useState(false);
  const [blog, setBlog] = useState();
  const [account, setAccount] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchBlog = async () => {
      const res = await postInspectApi.getPost(id);
      setBlog(res.payload);
      setAccount(res.payload.creator);
      setStatus(true);
    };

    fetchBlog();
  }, []);

  if (status == false) {
    return (
      <>
        <div>Loading....</div>
        <SkeletonBlogDetails />
      </>
    );
  }

  const handleAcceptPost = async () => {
    await postInspectApi
      .acceptPost(id)
      .then(() => {
        toast.success("Accept post is success");
      })
      .then(() => {
        navigate("/dashboard");
      })
      .catch((res) => {
        toast.success("Accept post is success");
        return navigate("/dashboard/browse-articles");
      });
  };

  const dataBlocks = JSON.parse(blog.blocks);

  const handleDeletePost = () => {};
  return (
    <>
      <section className="about__blog relative">
        <div className="container mx-auto md:w-[680px] md:my-20 xl:w-[680px] xl:my-20">
          <div className="">
            <h3 className="title__blog xl:font-extrabold xl:font-roboto xl:text-4xl font-roboto md:font-extrabold md:text-4xl">
              {blog.title}
            </h3>
          </div>

          <div className="account flex justify-start items-center gap-3 my-10">
            <div className="">
              <img
                src={account.avatarUrl}
                alt={""}
                className="rounded-full xl:w-[44px] xl:h-[44px] w-[44px] h-[44px] object-cover"
              />
            </div>

            <div className="flex flex-col">
              <div className="flex justify-start items-center gap-2">
                <h3>{account.nickname}</h3>
                <div className=""> - </div>
                <button className="text-lime-600">Follow</button>
              </div>

              <div className="flex justify-start items-center gap-2">
                <div className="text-sm text-gray-400">5 min read</div>

                <div className=""> - </div>

                {/* <div className="text-sm text-gray-400">{formattedDate}</div> */}
              </div>
            </div>
          </div>

          <div className="mt-6 py-3 border-t-[1px] border-t-gray-200 border-b-[1px] border-b-gray-200">
            <ActionBlogComponent
              upVote={blog.upvote}
              downVote={blog.downvote}
            />
          </div>

          <div className="content__blog my-6">
            <div className="content">
              <TextEditorOutput dataBlocks={dataBlocks} />
            </div>
          </div>

          <div className="xl:my-7">
            <TopicForBlog topics={blog.topics} />
          </div>

          <div className="xl:my-5">
            <ActionBlogComponent
              upVote={blog.upvote}
              downVote={blog.downvote}
            />
          </div>
        </div>
        <div className="sticky bottom-1/2 left-1/4">
          <div
            onClick={handleAcceptPost}
            className="px-4 py-3 bg-green-300 text-white w-fit cursor-pointer hover:bg-green-500 duration-200 ease-linear"
          >
            Accept
          </div>
          <div className="px-4 py-3 bg-red-300 text-white w-fit mt-5 duration-200 ease-linear hover:bg-red-500 cursor-pointer">
            Delete
          </div>
        </div>
      </section>
    </>
  );
}
