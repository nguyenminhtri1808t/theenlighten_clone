import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { publicAccountApi } from "../../apis/public/accountApi";
import { Each } from "../../utils/each";
import BlogVerticalItem from "../blog/blogVertical/blogVerticalItem";
import BlogCardItem from "../blog/blogCard";
import { SkeletonBlogCardList } from "../blog/loading";

AccountForBlogComponent.propTypes = {};

function AccountForBlogComponent({ account }) {
  const [status, setStatus] = useState(false);
  const [blogsAccount, setBlogsAccount] = useState([]);

  useEffect(() => {
    const fetchBlogsForAccount = async () => {
      const res = await publicAccountApi.getAllPostsForAccount(account.id);
      setBlogsAccount(res.payload.posts);
      setStatus(true);
    };

    fetchBlogsForAccount();
  }, []);

  return (
    <>
      <div className="about__account">
        <div className="">
          <img
            src={account.avatarUrl}
            alt={account.nickname}
            className="rounded-full md:w-[100px] md:h-[100px] xl:w-[100px] xl:h-[100px] object-cover "
          />
        </div>

        <div className="my-5 border-b-[1px] border-b-zinc-100">
          <div className="flex justify-between items-center">
            <div className="write__by">
              <h5 className="font-semibold text-2xl">
                Tác giả {account.nickname}
              </h5>
            </div>

            <div className="action__account flex items-center gap-3">
              <button className="btn__follow h-10 rounded-3xl leading-5 px-3 bg-green-700 text-white text-sm hover:bg-black transition-all">
                Theo dõi
              </button>
              <button className="btn__follow h-10 rounded-3xl leading-5 px-3 bg-green-700 text-white text-sm hover:bg-black transition-all">
                <svg
                  className="w-4 h-4 text-white dark:text-white "
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 20 21"
                >
                  <path
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M10 3.464V1.1m0 2.365a5.338 5.338 0 0 1 5.133 5.368v1.8c0 2.386 1.867 2.982 1.867 4.175C17 15.4 17 16 16.462 16H3.538C3 16 3 15.4 3 14.807c0-1.193 1.867-1.789 1.867-4.175v-1.8A5.338 5.338 0 0 1 10 3.464ZM1.866 8.832a8.458 8.458 0 0 1 2.252-5.714m14.016 5.714a8.458 8.458 0 0 0-2.252-5.714M6.54 16a3.48 3.48 0 0 0 6.92 0H6.54Z"
                  ></path>
                </svg>
              </button>
            </div>
          </div>

          <div className="follower mb-3">
            <span className="text-sm">100 Người theo dõi</span>
          </div>

          <div className="introduce">
            <span className="text-sm">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facere
              eius, dolores id voluptatum perferendis impedit ipsum velit error
              earum placeat iste omnis animi, accusantium doloribus eos nemo
              explicabo dolor reprehenderit?
            </span>
          </div>
        </div>
      </div>

      <div className="about__blogList py-7 border-t-zinc-300 border-t-[1px] border-b-zinc-300 border-b-[1px]">
        <div className="about__blogList-title mb-5">
          <h5 className="text-lg font-medium">
            Các bài viết khác của {account.nickname}
          </h5>
        </div>

        <div className="blog__list grid grid-cols-2 gap-6">
          {status ? (
            <Each
              of={blogsAccount}
              render={(blog, index) => (
                <BlogCardItem
                  blog={blog}
                  key={index}
                  accountName={account.nickname}
                  accountAvatar={account.avatarUrl}
                />
              )}
            />
          ) : (
            <SkeletonBlogCardList />
          )}
        </div>
      </div>

      <div className="mt-6">
        <button className="see__more p-3 border-[1px] border-black text-xs rounded-3xl hover:bg-black hover:text-white transition-all">
          Xem thêm bài viết của {account.nicknames}
        </button>
      </div>
    </>
  );
}

export default AccountForBlogComponent;
