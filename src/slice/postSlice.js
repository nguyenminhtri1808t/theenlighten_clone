import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  post: null,
  title: null,
  overview: null,
  backgroundUrl: null,
  topic: null,
};

const postSlice = createSlice({
  name: "post",
  initialState,
  reducers: {
    addPost: (state, action) => {
      state.backgroundUrl = action.payload.backgroundUrl;
      state.overview = action.payload.overview;
      state.post = action.payload.post;
      state.title = action.payload.title;
      state.topic = action.payload.topic;
    },
    removePost: (state) => {
      state.backgroundUrl = null;
      state.overview = null;
      state.post = null;
      state.title = null;
      state.topic = null;
    },
  },
});

export const postActions = postSlice.actions;
export const postReducer = postSlice.reducer;
