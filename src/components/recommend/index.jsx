import React, { Suspense } from "react";
import TopicBlogList from "../topic/topicBox";
import Help from "../help";

export const Recommend = ({ children }) => {
  return (
    <div className="xl:py-6 xl:border-t-2 px-10">
      <div className="container mx-auto gap-5 md:grid md:grid-cols-3 md:mb-7 lg:grid lg:grid-cols-3 lg:gap-8 xl:grid xl:grid-cols-3 xl:gap-20 2xl:container-[1200px]">
        <div className="relative md:order-2 md:col-span-1 lg:order-2 lg:col-span-1 xl:order-2 xl:col-span-1">
          <div className="md:sticky md:top-[100px] lg:sticky lg:top-[100px] xl:sticky xl:top-[100px]">
            <TopicBlogList />
            <div className="hidden lg:block xl:bloc 2xl:block">
              <Help />
            </div>
          </div>
        </div>
        <div className="md:order-1 md:col-span-2 lg:order-1 lg:col-span-2 xl:order-1 xl:col-span-2">
          {children}
        </div>
      </div>
    </div>
  );
};
