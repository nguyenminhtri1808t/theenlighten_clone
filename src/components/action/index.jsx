import React, { useState } from "react";
import PropTypes from "prop-types";
import { IconComment, IconDownVote, IconUpVote } from "../blog/icon";
import CommentComponent from "../boxComments";
import { votePrivateApi } from "../../apis/private/voteApi";
import { set } from "react-hook-form";
import toast from "react-hot-toast";

ActionBlogComponent.propTypes = {};

function ActionBlogComponent({
  upVote,
  downVote,
  handleShowModelComment,
  postId,
}) {
  const [valueUpVote, setValueUpVote] = useState(upVote);
  const [valueDownVote, setValueDownVote] = useState(downVote);
  const [checkUpVote, setCheckUpVote] = useState(false);
  const [checkDownVote, setCheckDownVote] = useState(false);
  const [modelComment, setModelComment] = useState(false);
  function toggleModelComment() {
    handleShowModelComment();
    setModelComment(!modelComment);
  }

  const handleUpVote = async () => {
    if (!checkUpVote) {
      await votePrivateApi.upVote(postId).then((res) => {
        if (checkDownVote) {
          setValueDownVote(valueDownVote - 1);
          setCheckDownVote(false);
        }
        setValueUpVote(valueUpVote + 1);
        setCheckUpVote(true);
        toast.success("Bạn đã up vote bài viết này thành công !");
      });
    } else {
      toast.error("Bạn đã up vote bài viết này rồi !");
    }
  };

  const handleDownVote = async () => {
    if (!checkDownVote) {
      await votePrivateApi.downVote(postId).then((res) => {
        if (checkUpVote) {
          setValueUpVote(valueUpVote - 1);
          setCheckUpVote(false);
        }
        setValueDownVote(valueDownVote + 1);
        setCheckDownVote(true);
        toast.success("Bạn đã down vote bài viết này thành công !");
      });
    } else {
      toast.error("Bạn đã down vote bài viết này rồi !");
    }
  };

  console.log("modelComment", postId);
  return (
    <>
      <div className="action flex justify-between items-center">
        <div className="flex items-center gap-5">
          <div className="like flex items-center gap-2">
            <div className="cursor-pointer" onClick={handleUpVote}>
              <IconUpVote />
            </div>

            <div className="value_like text-gray-400 text-sm cursor-pointer">
              {valueUpVote}
            </div>
          </div>

          <div className="like flex items-center gap-2">
            <div className="cursor-pointer" onClick={handleDownVote}>
              <IconDownVote />
            </div>

            <div className="value_like text-gray-400 text-sm cursor-pointer">
              {valueDownVote}
            </div>
          </div>
          <div className="like flex items-center gap-2">
            <div className="cursor-pointer " onClick={toggleModelComment}>
              <IconComment />
            </div>
          </div>
        </div>

        <div className="flex items-center gap-5">
          <div className="like flex items-center gap-2">
            <svg
              className="w-5 h-5 text-gray-400 hover:text-black cursor-pointer"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 14 20"
            >
              <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="m13 19-6-5-6 5V2a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1v17Z"
              ></path>
            </svg>
          </div>
          <div className="like flex items-center gap-2">
            <svg
              className="w-5 h-5 text-gray-400 hover:text-black cursor-pointer"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 16 18"
            >
              <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M8 1v11m0 0 4-4m-4 4L4 8m11 4v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-3"
              ></path>
            </svg>
          </div>
          <div className="like flex items-center gap-2">
            <svg
              className="w-5 h-5 text-gray-400 hover:text-black cursor-pointer"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 18"
            >
              <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="m1 14 3-3m-3 3 3 3m-3-3h16v-3m2-7-3 3m3-3-3-3m3 3H3v3"
              ></path>
            </svg>
          </div>
          <div className="like flex items-center gap-2">
            <svg
              className="w-5 h-5 text-gray-400 hover:text-black cursor-pointer"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 16 21"
            >
              <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M8 3.464V1.1m0 2.365a5.338 5.338 0 0 1 5.133 5.368v1.8c0 2.386 1.867 2.982 1.867 4.175C15 15.4 15 16 14.462 16H1.538C1 16 1 15.4 1 14.807c0-1.193 1.867-1.789 1.867-4.175v-1.8A5.338 5.338 0 0 1 8 3.464ZM4.54 16a3.48 3.48 0 0 0 6.92 0H4.54Z"
              ></path>
            </svg>
          </div>
        </div>
      </div>

      <div
        className={`${
          modelComment
            ? "fixed translate-x-0 opacity-100"
            : "hidden translate-x-[500px]"
        }  opacity-0 duration-300 linear top-0 right-0 bottom-0 w-1/3 h-full bg-white shadow-lg py-14 p-5 overflow-scroll`}
      >
        <div className={`${modelComment ? "block" : "hidden"}`}>
          <CommentComponent postId={postId} />
        </div>
      </div>
    </>
  );
}

export default ActionBlogComponent;
