import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { publicPostsApi } from "../../apis/public/postsApi";
import { Each } from "../../utils/each";
import TrendingItem from "./trendingItem";
import toast from "react-hot-toast";

TrendingListComponent.propTypes = {};

function TrendingListComponent(props) {
  const [dataTrending, setDataTrending] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [errors, setErrors] = useState(null);

  useEffect(() => {
    const fetchBlogsTrending = async () => {
      try {
        const res = await publicPostsApi.getPostsTrending();
        setDataTrending(res.payload);
      } catch (error) {
        setErrors(error);
        toast.error("Lỗi tải dữ liệu, tải dữ liệu trending không thành công.");
      } finally {
        setLoading(false);
      }
    };

    fetchBlogsTrending();
  }, []);

  return (
    <section className="py-[40px] px-14">
      <div className="container mx-auto">
        <div className="flex justify-start items-center gap-3 mb-3">
          <div className="">
            <svg
              width="38"
              height="39"
              viewBox="0 0 28 29"
              fill="none"
              className="ji ah"
            >
              <path fill="#fff" d="M0 .8h28v28H0z"></path>
              <g opacity="0.8" clipPath="url(#trending_svg__clip0)">
                <path fill="#fff" d="M4 4.8h20v20H4z"></path>
                <circle cx="14" cy="14.79" r="9.5" stroke="#000"></circle>
                <path
                  d="M5.46 18.36l4.47-4.48M9.97 13.87l3.67 3.66M13.67 17.53l5.1-5.09M16.62 11.6h3M19.62 11.6v3"
                  stroke="#000"
                  strokeLinecap="round"
                ></path>
              </g>
              <defs>
                <clipPath id="trending_svg__clip0">
                  <path
                    fill="#fff"
                    transform="translate(4 4.8)"
                    d="M0 0h20v20H0z"
                  ></path>
                </clipPath>
              </defs>
            </svg>
          </div>
          <h4 className="font-medium">Xu hướng trên enlighten .</h4>
        </div>

        <div className="grid grid-cols-1 gap-7 py-3 md:grid-cols-2 md:gap-8 lg:grid-cols-3 xl:grid-cols-3 xl:gap-8">
          {isLoading ? (
            <div>Loading...</div>
          ) : (
            <Each
              of={dataTrending}
              render={(blog, index) => (
                <TrendingItem item={blog} index={index} key={index} />
              )}
            />
          )}
          {errors && <div>{errors}</div>}
        </div>
      </div>
    </section>
  );
}

export default TrendingListComponent;
