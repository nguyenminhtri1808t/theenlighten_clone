import { useEffect, useState } from "react";
import { publicAccountApi } from "../../apis/public/accountApi";
import { SkeletonBlogVerticalList } from "../blog/loading";
import { Each } from "../../utils/each";
import BlogVerticalItem from "../blog/blogVertical/blogVerticalItem";

export default function BlogForAccountComponent({ user }) {
  const [blogs, setBlogs] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [errors, setErrors] = useState(null);

  useEffect(() => {
    const fetchBlogsForAccount = async () => {
      setLoading(true);
      setErrors(null);
      try {
        const res = await publicAccountApi.getAllPostsForAccount(user.id);
        const blogs = res.payload.posts;
        setBlogs(blogs);
      } catch (error) {
        setErrors(error);
      } finally {
        setLoading(false);
      }
    };

    fetchBlogsForAccount();
  }, []);


  if (isLoading === true) {
    return <SkeletonBlogVerticalList />;
  }

  return (
    <>
      <div className="flex flex-col justify-start items-center gap-8">
        <Each
          of={blogs}
          render={(blog, index) => <BlogVerticalItem blog={blog} creator={user} key={index} />}
        />
      </div>
    </>
  );
}
