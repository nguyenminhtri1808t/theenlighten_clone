import axiosClient from "../axiosClient";

const urlBase = "/api/v1/posts";

export const postInspectApi = {
  getAllPostInspect: () => {
    const url = `${urlBase}/inspect`;
    return axiosClient.get(url);
  },

  getPost: (id) => {
    const url = `${urlBase}/${id}/inspect`;
    return axiosClient.get(url);
  },
  acceptPost: (id) => {
    const url = `${urlBase}/inspect/approve`;
    return axiosClient.put(url, {
      postId: id,
    });
  },
};
