import axiosClient from "../axiosClient";

const publicApi = "/public/api/v1/accounts";

export const publicAccountApi = {
  getAllPostsForAccount: async (id) => {
    try {
      const url = `${publicApi}/${id}/posts`;
      return await axiosClient.get(url);
    } catch (error) {
      console.error("Error fetching posts:", error);
      throw error;
    }
  },
};
