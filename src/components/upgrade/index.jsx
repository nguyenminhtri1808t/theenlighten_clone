import React from "react";
import ProfileDetail from "../profileDetail";
import { useAppSelector } from "../../hooks/reduxHook";
import { Navigate } from "../../assets/svg";

const UpgradeMember = () => {
  const feature = [
    {
      title: "Nâng cấp tài khoản",
      subtitle: "",
      content: (
        <div className="rotate-45">
          <Navigate />
        </div>
      ),
    },
  ];

  return (
    <div className="space-y-8 pt-4">
      {feature.map((item, index) => (
        <ProfileDetail
          key={index}
          title={item.title}
          index={index}
          subtitle={item.subtitle}
          content={item.content}
        />
      ))}
    </div>
  );
};

export default UpgradeMember;
