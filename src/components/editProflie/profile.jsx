import React from "react";
import ProfileDetail from "../profileDetail";
import { useAppSelector } from "../../hooks/reduxHook";

const Profile = () => {

  const user = useAppSelector((state) => state.authentication.user);

  const ProfileAccount = () => {
    return (
      <div className="flex items-center">
        <p>{user.nickname}</p>
        <img
          className="w-7 h-7 ml-2 rounded-full"
          src={user.avatarUrl}
          alt=""
        />
      </div>
    );
  };

  const feature = [
    { title: "Địa chỉ email", subtitle: "", content: user.authId },
    { title: "Tên người dùng ", subtitle: "", content: user.nickname },
    {
      title: "Thông tin cá nhân",
      subtitle: "Chỉnh sửa ảnh, tên, tiểu sử, v.v.",
      content: <ProfileAccount />,
    },
  ];

  return (
    <div className="space-y-8 pt-4">
      {feature.map((item, index) => (
        <ProfileDetail
          key={index}
          title={item.title}
          index={index}
          subtitle={item.subtitle}
          content={item.content}
        />
      ))}
    </div>
  );
};

export default Profile;
