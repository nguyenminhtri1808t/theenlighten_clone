import axiosClient from "./axiosClient";

export const ProfileApi = {
  updateProfile: async (data) => {
    const param = {
      avatarUrl: data.avatarUrl,
      nickname: data.nickname,
    };

    const url = "/api/v1/accounts";

    return await axiosClient.put(url, param, {
      headers: {
        "Content-Type": "application/json",
      },
    });
  },
};
