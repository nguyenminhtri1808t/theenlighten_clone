import axiosClient from "../axiosClient";

export const commentsPrivateApi = {
  postComment: async (data) => {
    try {
      const url = `/api/v1/comments`;
      return axiosClient.post(url, data);
    } catch (error) {
      console.error("Error posting comment:", error);
      throw error;
    }
  },
};
