import { Each } from "../../../utils/each";

export default function TopicForBlog({ topics }) {
  const listTopic = topics.split(",");
  console.log(listTopic);
  return (
    <div className="flex justify-start items-center gap-4 max-w-full flex-wrap">
      <Each
        of={listTopic}
        render={(topic, index) => (
          <div
            key={index}
            className="topic__item py-2 px-3 bg-zinc-200 cursor-pointer hover:bg-black hover:text-white transition-all  text-sm rounded-2xl capitalize"
          >
            {topic}
          </div>
        )}
      />
    </div>
  );
}
