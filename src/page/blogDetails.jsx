import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router";
import { publicPostsApi } from "../apis/public/postsApi";
import { set } from "react-hook-form";
import ActionBlogComponent from "../components/action";
import TopicForBlog from "../components/topic/topicForBlog";
import AccountForBlogComponent from "../components/accountForBlog";
import { Each } from "../utils/each";
import BlogCardItem from "../components/blog/blogCard";
import SkeletonBlogDetails from "../skeleton/pageBlogDetails";
import TextEditorOutput from "../components/textEditorOutput";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { auth } from "../firebase";
import { authenticationApi } from "../apis/public/authenticationApi";
import { authenticationActions } from "../slice/authenticationSlice";
import { useAppDispatch } from "../hooks/reduxHook";
import toast from "react-hot-toast";

BlogDetailsPage.propTypes = {};

function BlogDetailsPage() {
  const { id } = useParams();
  const [blog, setBlog] = useState({});
  const [status, setStatus] = useState();
  const [message, setMessage] = useState("");
  const [modelComment, setModelComment] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const user = useSelector((state) => state.authentication.user);
  const dispatch = useAppDispatch();

  useEffect(() => {
    const fetchBlog = async () => {
      try {
        const res = await publicPostsApi.getPost(id);
        const data = await res;
        setBlog(data.payload);
        setMessage(data.message);
      } catch (error) {
        setStatus(error.response.status);
      } finally {
        setLoading(false);
      }
    };

    fetchBlog();
  }, []);

  const handleSignIn = async () => {
    const provider = new GoogleAuthProvider();
    const clientToken = localStorage.getItem("vapidToken");
    await signInWithPopup(auth, provider)
      .then(async (response) => {
        const cleanedToken = response.user.accessToken;
        localStorage.setItem("token", cleanedToken);
        await authenticationApi
          .login(clientToken)
          .then((response) => {
            if (response) toast("Đăng nhập thành công", { icon: "🔶" });
            dispatch(
              authenticationActions.login({
                user: response.payload,
                token: cleanedToken,
                timeToken: new Date().getTime(),
              })
            );
          })
          .catch((error) => {
            console.log(error);
            toast.error("Đăng nhập không thành công");
          });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const date = new Date(blog.dateCreate);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;

  const account = blog.creator;
  const blogs = account;

  if (isLoading === true) {
    return (
      <>
        <SkeletonBlogDetails />
      </>
    );
  }

  const handleShowModelComment = () => {
    setModelComment(!modelComment);
  };

  return (
    <>
      {status === 500 ? (
        <div className="container w-[600px] mx-auto my-20 flex justify-center items-center flex-col">
          <h3 className="text-center text-2xl font-bold  italic">
            Bài viết này dành cho những tài khoản Premium
          </h3>
          {user == null ? (
            <button className="mt-5 px-4 py-3 bg-black text-white rounded" onClick={handleSignIn}>Đăng nhập</button>
          ) : (
            <Link
              to={"/member"}
              className="text-center text-xl pt-5 underline"
            >
              Nâng cấp tài khoản ngay
            </Link>
          )}
        </div>
      ) : (
        <>
          <section
            className={`about__blog ${
              modelComment ? "mr-[200px]" : " mr-0"
            } duration-300 linear`}
          >
            <div className="container mx-auto md:w-[680px] md:my-20 xl:w-[680px] xl:my-20">
              <div className="">
                <h3 className="title__blog xl:font-extrabold xl:font-roboto xl:text-4xl font-roboto md:font-extrabold md:text-4xl">
                  {blog.title}
                </h3>
              </div>

              <div className="account flex justify-start items-center gap-3 my-10">
                <div className="">
                  <img
                    src={account.avatarUrl}
                    alt={""}
                    className="rounded-full xl:w-[44px] xl:h-[44px] w-[44px] h-[44px] object-cover"
                  />
                </div>

                <div className="flex flex-col">
                  <div className="flex justify-start items-center gap-2">
                    <h3>{account.nickname}</h3>
                    <div className=""> - </div>
                    <button className="text-lime-600">Follow</button>
                  </div>

                  <div className="flex justify-start items-center gap-2">
                    <div className="text-sm text-gray-400">5 min read</div>

                    <div className=""> - </div>

                    <div className="text-sm text-gray-400">{formattedDate}</div>
                  </div>
                </div>
              </div>

              <div className="mt-6 py-3 border-t-[1px] border-t-gray-200 border-b-[1px] border-b-gray-200">
                <ActionBlogComponent
                  upVote={blog.upvote}
                  downVote={blog.downvote}
                  postId={id}
                  handleShowModelComment={handleShowModelComment}
                />
              </div>

              <div className="content__blog my-6">
                <div className="content">
                  <TextEditorOutput dataBlocks={JSON.parse(blog.blocks)} />
                </div>
              </div>

              <div className="xl:my-7">
                <TopicForBlog topics={blog.topics} />
              </div>

              <div className="xl:my-5">
                <ActionBlogComponent
                  upVote={blog.upvote}
                  downVote={blog.downvote}
                  postId={id}
                  handleShowModelComment={handleShowModelComment}
                />
              </div>
            </div>
          </section>

          <section className="about__author">
            <div className="py-16 bg-zinc-50">
              <div
                className={`container w-[680px] mx-auto ${
                  modelComment ? "mr-[700px]" : " mx-auto"
                } duration-300 linear`}
              >
                <AccountForBlogComponent account={account} />
              </div>
            </div>
          </section>
        </>
      )}
    </>
  );
}

export default BlogDetailsPage;
