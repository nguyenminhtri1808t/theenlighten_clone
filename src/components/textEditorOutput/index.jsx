import CheckList from "@editorjs/checklist";
import Code from "@editorjs/code";
import Delimiter from "@editorjs/delimiter";
import EditorJS from "@editorjs/editorjs";
import Embed from "@editorjs/embed";
import Header from "@editorjs/header";
import ImageTool from "@editorjs/image";
import InlineCode from "@editorjs/inline-code";
import List from "@editorjs/list";
import Marker from "@editorjs/marker";
import Table from "@editorjs/table";
import { memo, useEffect, useRef } from "react";
import { imageService } from "../../apis/private/imageApi";

function TextEditorOutput({ dataBlocks }) {
  const editorRef = useRef(null);

  const initEditor = () => {
    const editor = new EditorJS({
      data: dataBlocks,
      readOnly: true,
      placeholder: "Nội dung bài viết của bạn",
      holder: editorRef.current,
      onChange: () => {
        editor
          .save()
          .then((outputData) => {
            save(false);
            localStorage.setItem("data", JSON.stringify(outputData));
            setTimeout(() => {
              save(true);
            }, 1000);
          })
          .catch((error) => {
            console.log("Saving failed: ", error);
          });
      },
      onReady: () => {
        editorRef.current = editor;
      },
      tools: {
        // attaches: {
        //   class: Attaches,
        //   config: {
        //     uploader: {
        //       uploadByFile(file: File) {
        //         return {
        //           success: 1,
        //           file: {
        //             url: 'https://drive.google.com/user/catalog/my-file.pdf',
        //             size: file.size,
        //             name: file.name,
        //             extension: getFileType(file.name),
        //             title: file.name,
        //           },
        //         };
        //       },
        //     },
        //   },
        // },
        checkList: CheckList,
        code: Code,
        delimiter: Delimiter,
        inlineCode: InlineCode,
        marker: Marker,
        header: {
          class: Header,
          inlineToolbar: ["link"],
          config: {
            levels: [1, 2, 3, 4, 5, 6],
            defaultLevel: 1,
            placeholder: "Tiêu đề",
          },
        },
        list: {
          class: List,
          inlineToolbar: true,
        },
        table: {
          class: Table,
          inlineToolbar: true,
          config: {
            rows: 2,
            cols: 3,
          },
        },
        embed: Embed,
        // embed: {
        //   class: Embed,
        //   config: {
        //     services: {
        //       youtube: true,
        //       coub: true,
        //       twitter: true,
        //       Facebook: true,
        //       Instagram: true,
        //       codepen: {
        //         regex: /https?:\/\/codepen.io\/([^\/\?\&]*)\/pen\/([^\/\?\&]*)/,
        //         embedUrl:
        //           'https://codepen.io/<%= remote_id %>?height=300&theme-id=0&default-tab=css,result&embed-version=2',
        //         html: "<iframe height='300' scrolling='no' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'></iframe>",
        //         height: 300,
        //         width: 600,
        //         id: (groups: any) => groups.join('/embed/'),
        //       },
        //     },
        //   },
        // },
        image: {
          class: ImageTool,
          config: {
            uploader: {
              uploadByFile: async (file) => {
                let image;
                await imageService
                  .postImages(file)
                  .then((response) => {
                    image = response.payload;
                  })
                  .catch((error) => {
                    console.log(error);
                  });
                return {
                  success: 1,
                  file: {
                    url: image,
                  },
                };
              },
            },
          },
        },
      },
    });
  };

  useEffect(() => {
    if (editorRef.current === null) {
      initEditor();
    }

    return () => {
      editorRef?.current?.destroy();
      editorRef.current = null;
    };
  }, [editorRef.current]);

  return (
    <div>
      <div className="content">
        <div id="editorjs" className="text-xl" />
      </div>
    </div>
  );
}

export default memo(TextEditorOutput);
