import imageEmptyData from "../../assets/images/emptydata2.jpg"

export function EmptyDataComponent() {
  return <div className="w-[300px] mx-auto container my-44 text-center">
        <img src={imageEmptyData} alt="" className="w-full h-full object-cover object-center"/>
        <div><span className="italic">Hiện tại chưa có bài viết</span> <span>🤔 ​</span></div>
  </div>;
}
