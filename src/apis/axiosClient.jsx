import axios from "axios";
import toast from "react-hot-toast";

const baseURL = "https://efb7-2402-800-63ae-802e-f5f0-9710-a70c-5e4f.ngrok-free.app";

const axiosClient = axios.create({
  baseURL,
  headers: {
    "Content-Type": "application/json",
  },
  paramsSerializer: (params) => queryString.stringify(params)
});

axiosClient.interceptors.request.use(async (config) => {
  const token = localStorage.getItem("token");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

axiosClient.interceptors.response.use(
  (response) => {
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  (error) => {
    if (error.response && error.response.status === 401) {
      toast("Vui lòng đăng nhập lại", { icon: "🔶" });
    }
    throw error;
  }
);

export default axiosClient;
