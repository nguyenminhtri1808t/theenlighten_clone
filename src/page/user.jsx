import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Lock, Member } from "../assets/svg";
import { useAppSelector } from "../hooks/reduxHook";
import { publicAccountApi } from "../apis/public/accountApi";

export const UserPage = () => {
  const [tab, setTab] = useState("profile");
  const [user, setUser] = useState(null);
  const id = useParams();

  const handleTab = (tab) => {
    setTab(tab);
  };

  useEffect(() => {
    const getUser = async () => {
      await publicAccountApi
        .getAllPostsForAccount(id)
        .then((response) => {
          setUser(response.payload);
        })
        .catch((error) => {
          console.log(error);
        });
    };
    getUser();
  }, []);

  return (
    <div className="grid grid-cols-3">
      <div className="col-span-2 py-10 pl-48 pr-32">
        <header className="mb-6">
          {/* <p className="text-5xl font-semibold">{user.nickname}</p> */}
        </header>
        <div className="content">
          <div className="mb-12 border-b border-gray-200 dark:border-gray-700">
            <ul className="flex flex-wrap -mb-px text-sm font-medium text-center">
              <li className="me-2" role="presentation">
                <button
                  onClick={() => handleTab("profile")}
                  className={` ${
                    tab === "dashboard"
                      ? "hover:text-gray-600 hover:bg-gray-50 hover:border-gray-900 dark:hover:text-gray-900"
                      : ""
                  } inline-block py-2.5 px-5 border-0 rounded-t-lg `}
                  type="button"
                >
                  Bài viết
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="py-10 pl-10">
        <div className="relative w-fit">
          {/* <img className="rounded-full" src={user.avatarUrl} alt="" /> */}
          <div className="absolute bottom-0 right-0">
            <Member />
          </div>
        </div>
        {/* <p className="font-medium my-2">{user.nickname}</p> */}
        <Link className="text-green-700">Chỉnh sửa thông tin cá nhân</Link>
      </div>
    </div>
  );
};
