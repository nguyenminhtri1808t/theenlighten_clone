import axiosClient from "../axiosClient";

export const votePrivateApi = {
  upVote: async (id) => {
    const url = "/api/v1/account-posts-reaction/reaction";
    return axiosClient.post(url, {
      idPost: id,
      isVote: "true",
    });
  },
  downVote: async (id) => {
    const url = "/api/v1/account-posts-reaction/reaction";
    return axiosClient.post(url, {
      idPost: id,
      isVote: "false",
    });
  },
};
