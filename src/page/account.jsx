import React, { useReducer, useState } from "react";
import { Link } from "react-router-dom";
import { Lock, Member } from "../assets/svg";
import { useAppSelector } from "../hooks/reduxHook";
import BlogForAccountComponent from "../components/blogForAccount";

export const AccountPage = () => {
  const [tab, setTab] = useState("profile");
  const user = useAppSelector((state) => state.authentication.user);

  const handleTab = (tab) => {
    setTab(tab);
  };

  return (
    <div className="grid grid-cols-3">
      <div className="col-span-2 py-10 pl-48 pr-32">
        <header className="mb-6">
          <p className="text-5xl font-semibold">{user.nickname}</p>
        </header>
        <div className="content">
          <div className="mb-12 border-b border-gray-200 dark:border-gray-700">
            <ul className="flex flex-wrap -mb-px text-sm font-medium text-center">
              <li className="me-2" role="presentation">
                <button
                  onClick={() => handleTab("profile")}
                  className={` ${
                    tab === "profile"
                      ? "bg-gray-100"
                      : "hover:text-gray-600 hover:bg-gray-50 hover:border-gray-900 dark:hover:text-gray-900"
                  } inline-block py-2.5 px-5 border-0 rounded-t-lg `}
                  type="button"
                >
                  Danh mục
                </button>
              </li>
              <li className="me-2" role="presentation">
                <button
                  onClick={() => handleTab("dashboard")}
                  className={` ${
                    tab === "dashboard"
                      ? "bg-gray-100"
                      : "hover:text-gray-600 hover:bg-gray-50 hover:border-gray-900 dark:hover:text-gray-900"
                  } inline-block py-2.5 px-5 border-0 rounded-t-lg `}
                  type="button"
                >
                  Thông tin
                </button>
              </li>
              <li className="me-2" role="presentation">
                <button
                  onClick={() => handleTab("blogs")}
                  className={` ${
                    tab === "blogs"
                      ? "bg-gray-100"
                      : "hover:text-gray-600 hover:bg-gray-50 hover:border-gray-900 dark:hover:text-gray-900"
                  } inline-block py-2.5 px-5 border-0 rounded-t-lg `}
                  type="button"
                >
                  Danh sách bài viết
                </button>
              </li>
            </ul>
          </div>
          <div id="default-tab-content" className="mb-12">
            <div
              className={`${
                tab === "profile" ? "" : " hidden"
              } p-4 rounded-lg bg-gray-50`}
              id="profile"
            >
              <div className="flex justify-between ">
                <div className="space-y-3 px-8 pt-4">
                  <div className="flex items-center space-x-2">
                    <img
                      className="w-8 h-8 rounded-full"
                      src="https://i.pinimg.com/236x/18/7f/65/187f656be22bf834ae896e60485ddd41.jpg"
                      alt=""
                    />
                    <p className="text-sm font-medium">Ltvy</p>
                  </div>
                  <p className="font-bold text-lg">Bộ sưu tập</p>
                  <p className="text-xs flex ">
                    3 bài viết{" "}
                    <span className="ml-2">
                      <Lock />
                    </span>
                  </p>
                </div>
                <div className="h-40 flex overflow-hidden rounded-r">
                  <img
                    className="w-40"
                    src="https://i.pinimg.com/564x/e5/63/b7/e563b7005f806e37ba4bc05c719f1f8c.jpg"
                    alt=""
                  />
                  <img
                    className="w-20 object-cover"
                    src="https://i.pinimg.com/236x/f9/dd/79/f9dd79904b132381ca4c53bb2074a8d0.jpg"
                    alt=""
                  />
                  <img
                    className="w-10 object-cover"
                    src="https://i.pinimg.com/236x/2d/73/8d/2d738d27be77073dd518b7e5a61d1216.jpg"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div
              className={`${
                tab === "dashboard" ? "" : " hidden"
              } p-4 rounded-lg bg-gray-50`}
              id="dashboard"
              role="tabpanel"
              aria-labelledby="dashboard-tab"
            >
              <div className="h-64 flex items-center">
                <div className="text-sm text-gray-500 dark:text-gray-400 text-center px-28 space-y-4">
                  <p className="font-medium text-black">
                    Nói với thế giới về bản thân bạn
                  </p>
                  <p className="text-xs">
                    Đây là nơi bạn có thể chia sẻ thêm về bản thân: lịch sử,
                    kinh nghiệm làm việc, thành tích, sở thích, ước mơ, v.v. Bạn
                    thậm chí có thể thêm hình ảnh và sử dụng văn bản có định
                    dạng để cá nhân hóa tiểu sử của mình.
                  </p>
                  <button className="p-2 px-5 rounded-full border-gray-900 text-gray-900 font-medium border">
                    Bắt đầu
                  </button>
                </div>
              </div>
            </div>
            <div
              className={`${
                tab === "blogs" ? "" : " hidden"
              } p-4 rounded-lg bg-gray-50`}
              id="blogs"
              role="tabpanel"
              aria-labelledby="blogs-tab"
            >
              <div className="mt-10">
                <BlogForAccountComponent user={user} />
              </div>
            </div>
          </div>
          <hr className="mb-12" />
          <Link
            href={"/account/following"}
            className="text-sm font-medium text-lime-600"
          >
            2 Người theo dõi
          </Link>
        </div>
      </div>
      <div className="py-10 pl-10">
        <div className="relative w-fit">
          <img className="rounded-full" src={user.avatarUrl} alt="" />
          <div className="absolute bottom-0 right-0">
            <Member />
          </div>
        </div>
        <p className="font-medium my-2">{user.nickname}</p>
        <Link className="text-green-700">Chỉnh sửa thông tin cá nhân</Link>
      </div>
    </div>
  );
};
