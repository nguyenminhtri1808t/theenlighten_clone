import { createSlice } from "@reduxjs/toolkit";

const NavigateSlice = createSlice({
  name: "navigate",
  initialState: {
    navigate: null,
  },
  reducers: {
    addNavigate: (state, action) => {
      state.navigate = action.payload.navigate;
    },
    removeNavigate: (state) => {
      state.navigate = null;
    },
  },
});

export const NavigateActions = NavigateSlice.actions;
export const navigateReducer = NavigateSlice.reducer;
