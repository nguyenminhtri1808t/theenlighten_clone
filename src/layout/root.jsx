import React from "react";
import PropTypes from "prop-types";
import HeaderNotUser from "../components/headerNotUser";
import { Outlet } from "react-router-dom";
import Help from "../components/help";
import HeaderUserComponent from "../components/headerLogin";
import { useAppSelector } from "../hooks/reduxHook";

export const RootLayout = (props) => {
  // mock authentication status
  const auth = useAppSelector((state) => state.authentication.user);
  return (
    <>
      <div className="relative flex flex-col min-h-screen">
        {auth ? <HeaderUserComponent /> : <HeaderNotUser />}
        <main className="flex-grow">
          <Outlet />
        </main>
        {/* <footer className="flex-shrink-0">
          <Help />
        </footer> */}
      </div>
    </>
  );
};
