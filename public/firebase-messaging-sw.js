importScripts(
  "https://www.gstatic.com/firebasejs/9.0.0/firebase-app-compat.js"
);
importScripts(
  "https://www.gstatic.com/firebasejs/9.0.0/firebase-messaging-compat.js"
);

const firebaseConfig = {
  apiKey: "AIzaSyCPWmuFbzOP_s-0WmF4sOPzqCd7AH1MVKI",
  authDomain: "kaka-5c946.firebaseapp.com",
  projectId: "kaka-5c946",
  storageBucket: "kaka-5c946.appspot.com",
  messagingSenderId: "579392227763",
  appId: "1:579392227763:web:65b1ec29e56b946d2d63c6",
  measurementId: "G-LYVLJWNK7P",
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
  console.log("Received background message ", payload);
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
  };
  self.registration.showNotification(notificationTitle, notificationOptions);
});
