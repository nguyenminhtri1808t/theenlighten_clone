/* eslint-disable react/no-children-prop */
import { useEffect, useRef, useState } from "react";
import { publicPostsApi } from "../apis/public/postsApi";
import BannerComponent from "../components/banner";
import BlogVerticalItem from "../components/blog/blogVertical/blogVerticalItem";
import { SkeletonBlogVerticalList } from "../components/blog/loading";
import { Recommend } from "../components/recommend";
import TrendingListComponent from "../components/trending";
import { Each } from "../utils/each";

HomePage.propTypes = {};

function HomePage() {
  const [firstLoading, setFirstLoading] = useState(true);
  const [isLoading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [errors, setErrors] = useState(null);
  const [blogs, setBlogs] = useState([]);
  const observerTarget = useRef(null);

  const fetchBlogs = async () => {
    setLoading(true);
    setErrors(null);
    try {
      const res = await publicPostsApi.gelAllPosts(page);
      const newBlogs = res?.payload;
      setBlogs((prev) => [...prev, ...newBlogs]);
      setFirstLoading(false);
    } catch (error) {
      setErrors(error);
    } finally {
      setLoading(false);
      setFirstLoading(false);
    }
  };

  useEffect(() => {
    fetchBlogs(page);
  }, [page]);

  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting) {
          setPage((prev) => prev + 1);
        }
      },
      { threshold: 1 }
    );

    if (observerTarget.current) {
      observer.observe(observerTarget.current);
    }

    return () => {
      if (observerTarget.current) {
        observer.unobserve(observerTarget.current);
      }
    };
  }, []);

  return (
    <div>
      <BannerComponent />
      <TrendingListComponent />
      <Recommend
        children={
          <section className="py-[40px] lg:py-0 md:py-0 xl:py-0 2xl:py-0 ">
            <div className="xl:mx-0 xl:px-0 mt-10">
              <div className="flex flex-col justify-start items-center gap-8">
                {firstLoading && <SkeletonBlogVerticalList />}
                <Each
                  of={blogs}
                  render={(item, index) => (
                    <BlogVerticalItem
                      blog={item}
                      creator={item.creator}
                      key={index}
                    />
                  )}
                />

                {isLoading && <div>Loading...</div>}
                {errors && <p>Error: {errors.message}</p>}
                <div ref={observerTarget}></div>
              </div>
            </div>
          </section>
        }
      />
    </div>
  );
}
export default HomePage;
