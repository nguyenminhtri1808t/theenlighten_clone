import React from "react";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";

const withAuthRedirect = (WrappedComponent) => {
  const ComponentWithAuthRedirect = (props) => {
    const user = useSelector((state) => state.authentication.user);

    if (user === null) {
      return <Navigate to="/" replace />;
    }

    return <WrappedComponent {...props} />;
  };

  return ComponentWithAuthRedirect;
};

export default withAuthRedirect;
