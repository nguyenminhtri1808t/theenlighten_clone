export function SkeletonBlogCard() {
  return (
    <>
      <div>
        <div className="py-10 border-b-[1px] md:border-0 md:py-0">
          <div className="img__blog cursor-pointer ">
            <div className="w-full h-[250px] rounded-xl animate-pulse bg-gray-300"></div>
          </div>
          <div className="author mt-4 flex justify-start items-center gap-3 cursor-pointer">
            <div className="w-[30px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>

            <div className="w-[100px] h-[20px] rounded-lg animate-pulse bg-gray-300"></div>
          </div>
          <div className="title mt-3">
            <div className="w-full h-[20px] rounded-lg bg-gray-300"></div>
          </div>
          <div className="content mt-3">
            <div className="grid grid-cols-3 gap-4">
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
            </div>
          </div>
          <div className="mt-3">
            <div className="text-xs text-gray-600 flex justify-start items-center gap-3 lg:text-xs xl:text-xs">
              <div className="w-[30px] h-[10px] animate-pulse rounded bg-gray-300"></div>
              <div className="w-[10px] h-[10px] animate-pulse rounded bg-gray-300"></div>
              <div className="w-[30px] h-[10px] animate-pulse rounded bg-gray-300"></div>
            </div>
          </div>
          <div className="action flex justify-between items-center mt-2">
            <div className="flex items-center gap-5">
              <div className="like flex items-center gap-2">
                <svg
                  className="w-5 h-5 text-gray-500 cursor-pointer hover:text-black dark:text-white"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 21 21"
                >
                  <path
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="m6.072 10.072 2 2 6-4m3.586 4.314.9-.9a2 2 0 0 0 0-2.828l-.9-.9a2 2 0 0 1-.586-1.414V5.072a2 2 0 0 0-2-2H13.8a2 2 0 0 1-1.414-.586l-.9-.9a2 2 0 0 0-2.828 0l-.9.9a2 2 0 0 1-1.414.586H5.072a2 2 0 0 0-2 2v1.272a2 2 0 0 1-.586 1.414l-.9.9a2 2 0 0 0 0 2.828l.9.9a2 2 0 0 1 .586 1.414v1.272a2 2 0 0 0 2 2h1.272a2 2 0 0 1 1.414.586l.9.9a2 2 0 0 0 2.828 0l.9-.9a2 2 0 0 1 1.414-.586h1.272a2 2 0 0 0 2-2V13.8a2 2 0 0 1 .586-1.414Z"
                  />
                </svg>

                <div className="w-[30px] h-[10px] animate-pulse rounded bg-gray-300"></div>
              </div>

              <div className="like flex items-center gap-2">
                <svg
                  className="w-5 h-5 text-gray-500 dark:text-white cursor-pointer hover:text-black"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 20 18"
                >
                  <path
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M5 5h9M5 9h5m8-8H2a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h4l3.5 4 3.5-4h5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1Z"
                  />
                </svg>

                <div className="w-[30px] h-[10px] animate-pulse rounded bg-gray-300"></div>
              </div>
            </div>

            <div className="flex items-center gap-5">
              <div className="like flex items-center gap-2">
                <svg
                  className="w-5 h-5 text-gray-500 dark:text-white hover:text-black cursor-pointer"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 14 20"
                >
                  <path
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="m13 19-6-5-6 5V2a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1v17Z"
                  ></path>
                </svg>

                <svg
                  className="w-5 h-5 text-gray-500 dark:text-white hover:text-black cursor-pointer"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  id="more"
                >
                  <g data-name="Layer 2">
                    <g data-name="more-horizotnal">
                      <circle cx="12" cy="12" r="2"></circle>
                      <circle cx="19" cy="12" r="2"></circle>
                      <circle cx="5" cy="12" r="2"></circle>
                    </g>
                  </g>
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export function SkeletonBlogVertical() {
  return (
    <>
      <div className="grid grid-cols-3 gap-3 xl:gap-12 items-center w-full">
        <div className="col-span-2">
          <div className="flex justify-start items-center gap-3">
            <div className="w-[30px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>

            <h5 className="text-xs xl:text-sm font-roboto">
              <div className="block w-[50px] h-[10px] animate-pulse bg-gray-300"></div>
            </h5>
          </div>

          <div className="py-3">
            <h5 className="font-roboto text-base font-bold line-clamp-2 text-ellipsis xl:text-xl hover:text-amber-400">
              <div className="block w-[50px] h-[10px] animate-pulse bg-gray-300"></div>
            </h5>
            <div className="grid grid-cols-3 gap-4">
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
            </div>
          </div>

          <div className="">
            <div className="text-xs flex justify-start items-center gap-3 lg:text-xs xl:text-xs">
              <div className="block w-[30px] h-[10px] animate-pulse bg-gray-300"></div>
              <div className="concat-string"> - </div>
              <div className="block w-[50px] h-[10px] animate-pulse bg-gray-300"></div>
            </div>
          </div>
        </div>
        <div className="col-span-1 max-h-full">
          .
          <div className="w-[200px] h-[200px] rounded-xl animate-pulse bg-gray-300"></div>
        </div>
      </div>
    </>
  );
}

export function SkeletonBlogCardList() {
  return (
    <>
      <SkeletonBlogCard />
      <SkeletonBlogCard />
      <SkeletonBlogCard />
      <SkeletonBlogCard />
      <SkeletonBlogCard />
      <SkeletonBlogCard />
      <SkeletonBlogCard />
      <SkeletonBlogCard />
      <SkeletonBlogCard />
      <SkeletonBlogCard />
    </>
  );
}

export function SkeletonBlogVerticalList() {
  return (
    <>
      <SkeletonBlogVertical />
      <SkeletonBlogVertical />
      <SkeletonBlogVertical />
      <SkeletonBlogVertical />
      <SkeletonBlogVertical />
      <SkeletonBlogVertical />
      <SkeletonBlogVertical />
      <SkeletonBlogVertical />
      <SkeletonBlogVertical />
    </>
  );
}

export function SkeletonBlogDoubleCardList() {
  return (
    <>
      <SkeletonBlogCard />
      <SkeletonBlogCard />
    </>
  );
}

export function SkeletonImageBlog() {
  return (
    <>
      <div className="w-full h-[250px] bg-gray-200 animate-pulse"></div>
    </>
  );
}
