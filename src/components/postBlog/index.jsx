import { yupResolver } from "@hookform/resolvers/yup";
import React, { useCallback, useState } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { ReactTags } from "react-tag-autocomplete";
import * as yup from "yup";
import { useAppDispatch, useAppSelector } from "../../hooks/reduxHook";
import { imageService } from "../../apis/private/imageApi";
import { publicPostsApi } from "../../apis/public/postsApi";
import { useNavigate } from "react-router-dom";
import { postActions } from "../../slice/postSlice";

const DATA_SUGGEST = [
  { value: 1, label: "FrontEnd " },
  { value: 2, label: "BackEnd" },
  { value: 3, label: "FullStack" },
  { value: 4, label: "ReactJS" },
  { value: 5, label: "NextJS" },
  { value: 6, label: "NodeJS" },
  { value: 7, label: "TypeScript" },
  { value: 8, label: "Công nghệ" },
];

const schema = yup
  .object({
    title: yup.string().required().min(50).max(100),
    preview: yup.string().required().min(50).max(100),
  })
  .required();

const PostBlog = ({ selected, onSelected, onChangePost }) => {
  const post = useAppSelector((state) => state.post);
  const [loading, setLoading] = useState(false);
  const [maxTagsReached, setMaxTagsReached] = useState(false);
  const [urlImage, setUrlImage] = useState(post.backgroundUrl);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      title: post.title,
    },
  });

  const handleImage = async (e) => {
    await imageService
      .postImages(e.target.files[0])
      .then((response) => {
        setUrlImage(response.payload);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onAdd = useCallback(
    (tag) => {
      const tags = selected.map((item) => item.label);
      if (selected.length >= 5) {
        setMaxTagsReached(true);
        toast("Bạn chỉ được chọn tối đa 5 thể loại", { icon: "🔶" });
        return;
      }
      onSelected([...selected, tag]);
    },
    [selected]
  );

  const onDelete = useCallback(
    (tagIndex) => {
      if (selected.length < 5) {
        setMaxTagsReached(false);
      }
      const newSelected = [...selected];
      newSelected.splice(tagIndex, 1);
      onSelected(newSelected);
    },
    [selected]
  );

  const OnSubmit = (data) => {
    setLoading(true);
    const dataEditor = localStorage.getItem("data");
    const topic = selected.map((item) => item.label).join(",");
    if (!urlImage) {
      toast("Bạn chưa chọn ảnh bìa", { icon: "🔶" });
      return;
    }
    const dataBlog = {
      title: data.title,
      topics: topic,
      description: data.preview,
      backgroundUrl: urlImage,
      blocks: dataEditor,
      htmlString: "",
    };

    const post = async () => {
      await publicPostsApi.postBlog(dataBlog)
        .then((response) => {
          if (response.status !== 200) {
            return;
          }
          setLoading(false);
          navigate(`/`);
          localStorage.removeItem("data");
          dispatch(postActions.removePost());
        })
        .catch((error) => {
          console.log(error);
        });
      setLoading(false);
    };
    post();
  };

  const validateCloseModal = (data) => {
    const dataEditor = JSON.parse(localStorage.getItem("data") || "{}");
    dataEditor;
  };

  function CustomOption({ children, classNames, option, ...optionProps }) {
    const classes = [classNames.option, option.selected ? " hidden" : " mb-2"];

    return (
      <div className={classes.join(" ")} {...optionProps}>
        {children}
      </div>
    );
  }

  function CustomTag({ classNames, tag, ...tagProps }) {
    return (
      <div className="flex justify-center items-center gap-2 bg-gray-100 py-1 px-3.5 rounded-lg text-sm">
        <span className={classNames.tagName}>{tag.label} </span>
        <button type="button" className={classNames.tag} {...tagProps}>
          <svg
            className="w-2 h-2"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 14"
          >
            <path
              stroke="red"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
            />
          </svg>
        </button>
      </div>
    );
  }

  function CustomInput({ classNames, inputWidth, ...inputProps }) {
    return (
      <input
        className={classNames.input}
        style={{ width: inputWidth }}
        {...inputProps}
      />
    );
  }

  return (
    <div className="fixed inset-0 bg-white z-50 w-full overflow-x-hidden overflow-y-auto">
      <div className="flex justify-center items-center absolute top-0 left-0 bottom-0 right-0 bg-white">
        <div className="relative w-full max-w-4xl  min-h-fit">
          <div className="flex items-center justify-between p-4 w-full">
            <span className="font-medium">Xem trước bài đăng</span>
            <button
              onClick={() => onChangePost(false)}
              type="button"
              className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center"
            >
              <svg
                className="w-3 h-3"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 14"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                />
              </svg>
              <span className="sr-only">Đóng</span>
            </button>
          </div>
          <form action="" className="grid grid-cols-2 gap-10 p-4">
            <div className="space-y-4">
              <div className="flex items-center justify-center w-full">
                {urlImage ? (
                  <label
                    htmlFor="banner_post"
                    className={`flex flex-col items-center justify-center w-full h-64 'border-2 border-gray-300 border-dashed
                  rounded-xl cursor-pointer bg-white  hover:bg-gray-100`}
                  >
                    <div className="relative w-full h-full">
                      <img
                        className="h-full w-full rounded-lg"
                        src={urlImage}
                        alt=""
                      />
                      <div className="absolute top-2 right-2 space-x-2 flex">
                        <div className="bg-white rounded-lg px-3 py-2">
                          Thay đổi
                          <input
                            id="banner_post"
                            type="file"
                            className="hidden"
                            onChange={handleImage}
                          />
                        </div>
                      </div>
                    </div>
                  </label>
                ) : (
                  <label
                    htmlFor="banner_post"
                    className={`flex flex-col items-center justify-center w-full h-64 ${
                      urlImage
                        ? "border-2 border-gray-300 "
                        : "border-2 border-gray-300 border-dashed"
                    }  rounded-xl cursor-pointer bg-white  hover:bg-gray-100`}
                  >
                    <div className="flex flex-col items-center justify-center pt-5 pb-6">
                      <svg
                        className="w-8 h-8 mb-4 text-gray-500"
                        aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 20 16"
                      >
                        <path
                          stroke="currentColor"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2"
                        />
                      </svg>
                      <p className="mb-2 text-sm text-gray-500 ">
                        <span className="font-semibold">Click to upload</span>{" "}
                        or drag and drop
                      </p>
                      <p className="text-xs text-gray-500 ">
                        SVG, PNG, JPG or GIF (MAX. 800x400px)
                      </p>
                    </div>
                    <input
                      id="banner_post"
                      type="file"
                      className="hidden"
                      onChange={handleImage}
                    />
                  </label>
                )}
              </div>
              <div>
                <input
                  type="text"
                  id="title"
                  className="bg-white border border-gray-300 text-gray-900 text-sm font-bold rounded-xl focus:ring-gray-500 focus:border-gray-500 block w-full p-2.5"
                  placeholder="Tiêu đề bài viết"
                  {...register("title")}
                  required
                />
                <p className="text-red-600 font-medium mt-4 text-sm">
                  {errors.title?.message}
                </p>
              </div>
              <div>
                <input
                  type="text"
                  id="preview"
                  className="bg-white border border-gray-300 text-gray-900 text-sm rounded-xl focus:ring-gray-500 focus:border-gray-500 block w-full p-2.5"
                  placeholder="Mô tả ngắn cho bài viết"
                  {...register("preview")}
                  required
                />
                <p className="text-red-600 font-medium mt-4 text-sm">
                  {errors.preview?.message}
                </p>
              </div>
              <div>
                <span>
                  <span className="font-medium text-sm">Lưu ý:</span>
                  <span className="ml-1 text-xs">
                    Những thay đổi ở đây sẽ ảnh hưởng đến cách bài viết của bạn
                    xuất hiện ở những nơi công cộng như trang chủ của Enlighten
                    và trong hộp thư đến của người đăng ký — chứ không phải nội
                    dung của bài viết.
                  </span>
                </span>
              </div>
            </div>
            <div>
              <div className="space-y-4">
                <p className="mb-0">
                  Người đăng:
                  <span className="text-md font-medium ml-1">LuongTuanVy</span>
                </p>
                <p className="text-sm">
                  Thêm hoặc thay đổi chủ đề (tối đa 5) để người đọc biết câu
                  chuyện của bạn nói về điều gì
                </p>
                <div>
                  <ReactTags
                    noOptionsText="Thể loại không tồn tại"
                    labelText=""
                    selected={selected}
                    suggestions={DATA_SUGGEST}
                    onAdd={onAdd}
                    placeholderText="Thể loại"
                    onDelete={onDelete}
                    renderTag={CustomTag}
                    renderOption={CustomOption}
                  />
                </div>
                <p className="text-sm">
                  <a className="underline mr-1" href="">
                    Tìm hiểu thêm
                  </a>
                  về điều gì sẽ xảy ra với bài đăng của bạn khi bạn xuất bản.
                </p>
                <button
                  type="button"
                  onClick={handleSubmit(OnSubmit)}
                  className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-white focus:outline-none bg-green-700 rounded-full border border-gray-200 hover:bg-green-800 hover:text-white focus:z-10 focus:ring-4 focus:ring-gray-200"
                >
                  Đăng bài
                </button>
                <button
                  type="button"
                  className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-full border border-gray-200 hover:bg-gray-100 hover:text-gray-800 focus:z-10 focus:ring-4 focus:ring-gray-200"
                >
                  Hẹn lịch đăng bài
                </button>
              </div>
            </div>
          </form>
        </div>
        <div
          className={` ${
            loading ? "" : "hidden"
          } bg-gray-50/50 w-full h-full absolute`}
        />
      </div>
    </div>
  );
};

export default PostBlog;
