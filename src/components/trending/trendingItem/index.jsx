import { Link } from "react-router-dom";

export default function TrendingItem({ item, index }) {
  const date = new Date(item.dateCreate);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  const formattedDate = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;

  return (
    <>
      <Link to={`blogs/${item.id}`} className="flex justify-start items-start gap-3 cursor-pointer">
        <div className="text-3xl font-roboto font-medium text-gray-300 md:text-3xl lg:text:2xl xl:text-3xl">
          {index >= 10 ? id : `0${index + 1}`}
        </div>

        <div className="flex flex-col gap-2">
          <div className="flex justify-start items-center gap-3">
            <img
              className="w-[30px] h-[30px] rounded-full lg:w-[25px] lg:h-[25px] xl:w-[30px] xl:h-[30px]"
              src={item.creator.avatarUrl}
              alt="Rounded avatar"
            />

            <h5 className="text-xs lg:text-xs xl:text-sm">
              {item.creator.nickname}
            </h5>
          </div>

          <div className="text-base font-roboto font-semibold md:text-base lg:text-base xl:text-lg">
            {item.title}
          </div>

          <div className="text-xs flex justify-start items-center gap-3 md:text-xs lg:text-xs xl:text-xs">
            <span>{formattedDate}</span>
            <div className="concat-string"> - </div>
            <div className="time-reading">5 phút đọc</div>
          </div>
        </div>
      </Link>
    </>
  );
}
