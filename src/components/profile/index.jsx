import React from "react";
import { auth } from "../../firebase";
import { useAppDispatch } from "../../hooks/reduxHook";
import { authenticationActions } from "../../slice/authenticationSlice";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { ArrowRight } from "../../assets/svg";

const featureUser = [
  { name: "Thông tin cá nhân", link: "/me/settings" },
  { name: "Danh sách yêu thích", link: "" },
  { name: "Bài viết", link: "" },
  { name: "Thống kê", link: "" },
];

const featureUpdate = [
  { name: "Cài đặt tài khoản", link: "", icon: "" },
  { name: "Quản lý chủ đề", link: "", icon: "" },
  { name: "Nâng cấp tài khoản", link: "/member", icon: <ArrowRight /> },
];

const Profile = (props) => {
  const { onValidate } = props;
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const handleLogout = () => {
    auth.signOut();
    toast("Đăng xuất thành công", { icon: "🔶" });
    dispatch(authenticationActions.logout());
    navigate("/");
  };

  const handleValidate = (link) => {
    if (!onValidate) navigate(link);
  };

  return (
    <div className="z-50 my-4 w-64 list-none bg-white py-2 divide-gray-100 shadow rounded-xl">
      <ul className="py-1 text-gray-700" aria-labelledby="dropdown">
        {featureUser.map((item, index) => (
          <li key={index}>
            <p
              onClick={() => handleValidate(item.link)}
              className="block w-full text-start py-2 px-4 text-sm hover:bg-gray-100"
            >
              {item.name}
            </p>
          </li>
        ))}
      </ul>
      <hr className="my-2" />
      <ul className="py-1 text-gray-700" aria-labelledby="dropdown">
        {featureUpdate.map((item, index) => (
          <li key={index}>
            <p
              onClick={() => handleValidate(item.link)}
              className="flex justify-between w-full text-start py-2 px-4 text-sm hover:bg-gray-100"
            >
              {item.name}
              {item.icon}
            </p>
          </li>
        ))}
      </ul>
      <hr className="my-2" />
      <ul className="pb-1 text-gray-700" aria-labelledby="dropdown">
        <li>
          <p
            onClick={handleLogout}
            className="block py-2 w-full px-4 text-start text-sm hover:bg-gray-100"
          >
            Đăng xuất
          </p>
        </li>
      </ul>
    </div>
  );
};

export default Profile;
