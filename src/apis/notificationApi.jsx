import axiosClient from "./axiosClient";

export const notificationApi = {
  getNotifications: async () => {
    return await axiosClient.get("/api/v1/notifications");
  },
};
