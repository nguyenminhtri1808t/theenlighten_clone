import axiosClient from "../axiosClient";

export const imageService = {
  postImages: async (file) => {
    const formData = new FormData();
    formData.append("image", file);
    return await axiosClient.post("/api/v1/upload/image", formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
  },
};
