import {
  SkeletonBlogCardList,
  SkeletonBlogDoubleCardList,
} from "../../components/blog/loading";

export default function SkeletonPageBlogsOrBlogTopicAll() {
  return (
    <div className="container mx-auto topic_about lg:w-[1200px]">
      <div className="mx-auto py-3 capitalize md:mt-10 font-bold text-2xl md:text-3xl lg:text-4xl md:mb-16 w-[100px] h-[30px] bg-gray-300 animate-pulse"></div>
      <div className="flex justify-center items-center gap-1 text-xs text-gray-600 font-medium mb-5 md:text-base">
        <div className="w-[50px] h-[20px] bg-gray-300 animate-pulse"></div>
        <div className="w-[30px] h-[20px] bg-gray-300 animate-pulse"></div>
        <div className="w-[80px] h-[20px] bg-gray-300 animate-pulse"></div>
        <div className="w-[25px] h-[20px] bg-gray-300 animate-pulse"></div>
        <div className="w-[50px] h-[20px] bg-gray-300 animate-pulse"></div>
      </div>

      <div className="text-center md:mb-10">
        <button className="btn-follow py-2 px-4 text-white rounded-full w-[50px] h-[20px] bg-gray-300 animate-pulse"></button>
      </div>

      <div className="mb-10">
        <div className="grid grid-cols-1 gap-3 md:grid-cols-2 md:gap-x-5 md:gap-y-10 lg:grid-cols-2 lg:gap-10">
          <SkeletonBlogDoubleCardList />
        </div>
        <div className="pt-10 grid grid-cols-1 gap-3 md:grid-cols-2 md:gap-x-5 md:gap-y-10 lg:grid-cols-3 lg:gap-10">
          <SkeletonBlogCardList />
        </div>
      </div>
    </div>
  );
}
