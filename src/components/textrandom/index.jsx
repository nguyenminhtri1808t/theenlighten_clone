import React, { useState, useEffect } from "react";

const TextRandom = () => {
  const [grid, setGrid] = useState([]);

  useEffect(() => {
    // Khởi tạo lưới với 25x25 chữ E ban đầu
    const initialGrid = Array(15).fill(Array(22).fill("E"));
    setGrid(initialGrid);

    // Tạo hiệu ứng ẩn hiện chữ E sau mỗi 1 giây
    const interval = setInterval(() => {
      const newGrid = initialGrid.map((row) => {
        return row.map((cell) => (Math.random() < 0.5 ? "E" : " "));
      });
      setGrid(newGrid);
    }, 1400);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className="App">
      {grid.map((row, rowIndex) => (
        <div key={rowIndex} style={{ display: "flex" }}>
          {row.map((cell, cellIndex) => (
            <div
              key={cellIndex}
              className="w-8 h-8 font-semibold text-2xl font-mono"
            >
              {cell}
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

export default TextRandom;
