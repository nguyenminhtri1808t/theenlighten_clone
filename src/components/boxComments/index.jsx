import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { commentsPublicApi } from "../../apis/public/commentsApi";
import { useForm } from "react-hook-form";
import { commentsPrivateApi } from "../../apis/private/commentsApi";
import { Each } from "../../utils/each";
import CommentItem from "./commentItem";
import toast from "react-hot-toast";
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { auth } from "../../firebase";
import { authenticationApi } from "../../apis/public/authenticationApi";
import { useAppDispatch } from "../../hooks/reduxHook";
import { authenticationActions } from "../../slice/authenticationSlice";

export default function CommentComponent({ postId }) {
  const user = useSelector((state) => state.authentication.user);
  const [comments, setComments] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const dispatch = useAppDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  useEffect(() => {
    const fetchComments = async () => {
      const res = await commentsPublicApi.getComments(postId);
      const comments = res.payload;
      setComments(comments);
      setLoading(false);
    };

    fetchComments();
  }, []);

  const isEmptyComments = comments.length === 0 ? true : false;

  const handleCommentPost = (data) => {
    const dataRequest = {
      postId: postId,
      content: data.comment,
    };

    const postComment = async () => {
      const res = await commentsPrivateApi
        .postComment(dataRequest)
        .then((res) => {
          const comment = res.payload;
          console.log(comment)
          setComments([...comments, comment]);
          toast.success("Comment thành công !");
          reset();
        })
        .catch((err) => {
          if (err.response.status === 404) {
            toast.error("Comment không thành công !. Vui lòng thử lại sau...");
          }
        });
    };
    postComment();
  };

  const handleSignIn = async () => {
    const provider = new GoogleAuthProvider();
    const clientToken = localStorage.getItem("vapidToken");
    await signInWithPopup(auth, provider)
      .then(async (response) => {
        const cleanedToken = response.user.accessToken;
        localStorage.setItem("token", cleanedToken);
        await authenticationApi
          .login(clientToken)
          .then((response) => {
            if (response) toast("Đăng nhập thành công", { icon: "🔶" });
            dispatch(
              authenticationActions.login({
                user: response.payload,
                token: cleanedToken,
                timeToken: new Date().getTime(),
              })
            );
          })
          .catch((error) => {
            console.log(error);
            toast.error("Đăng nhập không thành công");
          });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <section class="bg-white py-8 lg:py-16 antialiaseds">
        <div class="max-w-2xl mx-auto px-4">
          <div class="flex justify-between items-center mb-6">
            <h2 class="text-lg lg:text-2xl font-bold text-gray-900">
              Nhận xét & Bàn luận bài viết
            </h2>
          </div>
          {user == null ? (
            <div className="py-6 bg-gray-300 rounded text-center">
              <p className="text-black italic">
                " Bạn chưa login vào trang web "
              </p>
              <button
                onClick={handleSignIn}
                className="px-5 py-3 text-white bg-black duration-150 ease-linear rounded-xl mt-3"
              >
                Đăng nhập
              </button>
            </div>
          ) : (
            <form class="mb-6" onSubmit={handleSubmit(handleCommentPost)}>
              <div class="py-2 px-4 mb-4 bg-white rounded-lg rounded-t-lg border border-gray-700">
                <label for="comment" class="sr-only">
                  Your comment
                </label>
                <textarea
                  id="comment"
                  rows="6"
                  class="px-0 w-full text-sm text-gray-900 border-0 focus:ring-0 focus:outline-none placeholder-gray-800"
                  placeholder="Write a comment..."
                  {...register("comment", {
                    required: "Comment không thể để trống !",
                  })}
                  required
                ></textarea>
              </div>
              <p>{errors.comment?.message}</p>
              <button class="inline-flex items-center py-2.5 px-4 text-xs font-medium text-center text-white bg-gray-700 rounded-lg focus:ring-4  focus:ring-primary-900 hover:bg-primary-800">
                Post comment
              </button>
            </form>
          )}

          {isEmptyComments ? (
            <div>Chua co comments</div>
          ) : (
            <Each
              of={comments}
              render={(comment, index) => <CommentItem comment={comment} />}
            />
          )}
        </div>
      </section>
    </>
  );
}
