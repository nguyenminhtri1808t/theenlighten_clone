import React, { useEffect, useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import { onMessageListener } from "../../firebase";
import { useDispatch } from "react-redux";
import { NotificationActions } from "../../slice/notificationSlice";

const Notification = () => {
  const [notification, setNotification] = useState({ title: "", body: "" });
  const dispatch = useDispatch();

  const notify = () => toast(<ToastDisplay />);

  const ToastDisplay = () => {
    return (
      <div className="flex items-center">
        <div className="flex flex-col">
          <div className="text-lg font-semibold">{notification.title}</div>
          <div className="text-sm">{notification.body}</div>
        </div>
      </div>
    );
  };

  useEffect(() => {
    if (notification.title !== "") {
      notify();
    }
  }, [notification]);

  onMessageListener()
    .then((payload) => {
      setNotification({
        title: payload.notification.title,
        body: payload.notification.body,
      });
    })
    .then(() => {
      const fecthData = async () => {
        await notificationApi
          .getNotifications()
          .then((respone) => {
            dispatch(NotificationActions.addNotification(respone.payload));
          })
          .catch((err) => {
            console.log(err);
          });
      };
      fecthData();
    })
    .catch((err) => console.log("failed: ", err));

  return <Toaster />;
};

export default Notification;
