import { createSlice } from "@reduxjs/toolkit";

const notificationSlice = createSlice({
  name: "notification",
  initialState: {
    notifications: [],
  },
  reducers: {
    addNotification(state, action) {
      state.notifications = action.payload;
    },
    removeNotification(state, action) {
      state.notifications = null;
    },
  },
});

export const NotificationActions = notificationSlice.actions;
export const notificationReducer = notificationSlice.reducer;
