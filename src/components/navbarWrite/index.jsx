import { useCallback, useEffect, useState } from "react";
import PostBlog from "../postBlog";
import toast from "react-hot-toast";
import { useAppDispatch, useAppSelector } from "../../hooks/reduxHook";
import Profile from "../profile";
import { postActions } from "../../slice/postSlice";
import { Link, Navigate, useNavigate } from "react-router-dom";

const featureUser = [
  { name: "Thông tin cá nhân", link: "" },
  { name: "Danh sách yêu thích", link: "" },
  { name: "Bài viết", link: "" },
  { name: "Thống kê", link: "" },
];

const NavbarWrite = ({ save, onChange, handleValidate }) => {
  const [saveData, setSaveData] = useState(true);
  const [selected, setSelected] = useState([]);
  const [post, setPost] = useState(false);
  const [action, setAction] = useState(false);
  const user = useAppSelector((state) => state.authentication.user);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (typeof save === "boolean") setSaveData(save);
  }, [save]);

  const onSelected = (selecteds) => {
    setSelected(selecteds);
  };

  const onChangePost = (value) => {
    if (value) {
      const data = JSON.parse(localStorage.getItem("data") || "{}");
      const coverImage = data.blocks.find((item) => item.type === "image");
      const title = data.blocks.find((item) => item.type === "header").data
        .text;
      const backgroundUrl = coverImage ? coverImage.data.file.url : "";
      dispatch(postActions.addPost({ title, backgroundUrl: backgroundUrl }));
    }
    setPost(value);
  };

  const onValidate = useCallback((link) => {
    const data = JSON.parse(localStorage.getItem("data"));
    if (data) {
      handleValidate(true, link);
    } else {
      navigate(link);
    }
  }, []);

  const validate = () => {
    const data = JSON.parse(localStorage.getItem("data"));
    if (!data) {
      toast("Bạn chưa nhập nội dung bài viết", { icon: "🔶" });
      return false;
    }
    if (data.blocks.length === 0) {
      toast("Bạn chưa nhập nội dung bài viết", { icon: "🔶" });
      return false;
    }
    if (data.blocks[0].type !== "header") {
      toast("Bạn chưa nhập tiêu đề bài viết", { icon: "🔶" });
      return false;
    } else if (data.blocks[0].data === "") {
      toast("Bạn chưa nhập tiêu đề bài viết", { icon: "🔶" });
      return false;
    }
    return true;
  };

  return (
    <nav className="bg-white py-2  sticky h-[76px] left-0 right-0 top-0 z-50">
      <div className="flex flex-wrap justify-between items-center h-full">
        <div className="pl-5 flex justify-start items-center">
          <button
            data-drawer-target="drawer-navigation"
            data-drawer-toggle="drawer-navigation"
            aria-controls="drawer-navigation"
            className="p-2 mr-6 text-gray-600 rounded-lg cursor-pointer md:hidden hover:text-gray-900 hover:bg-gray-100 focus:bg-gray-100 dark:focus:bg-gray-700 focus:ring-2 focus:ring-gray-100 dark:focus:ring-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
          >
            <svg
              aria-hidden="true"
              className="w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h6a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                clipRule="evenodd"
              ></path>
            </svg>
            <svg
              aria-hidden="true"
              className="hidden w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            </svg>
          </button>

          <div className="w-fit overflow-hidden h-12 flex items-center justify-center">
            <button onClick={() => onValidate("/")} className=" mr-4">
              <span className="self-center text-2xl text-primary font-sans font-semibold whitespace-nowrap  tracking-widest">
                enlighten <span className="">.</span>
              </span>
            </button>
          </div>
          <p className="text-sm mb-0 text-gray-500">
            {onChange && (saveData ? "Đã lưu" : "Đang lưu...")}
          </p>
        </div>

        <div className="pr-5 flex items-center justify-end lg:order-2">
          <button
            type="button"
            onClick={() => validate() && onChangePost(true)}
            className="text-white bg-green-700 hover:bg-green-800 focus:outline-none focus:ring-4 focus:ring-green-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
          >
            Đăng bài
          </button>
          <button
            type="button"
            onClick={() => setAction(!action)}
            className="flex relative mx-3 text-sm  rounded-full md:mr-0 focus:ring-0 dark:focus:ring-gray-600"
          >
            <img
              className="w-10 h-10 rounded-full"
              src={user.avatarUrl}
              alt="user photo"
            />
            <div
              className={`${
                action ? "" : "hidden"
              } absolute left-0 -translate-x-1/2 bottom-0 translate-y-full pt-2 `}
            >
              <Profile onValidate={onValidate} />
            </div>
          </button>
        </div>

        <div>
          {post && (
            <PostBlog
              selected={selected}
              onSelected={onSelected}
              onChangePost={onChangePost}
            />
          )}
        </div>
      </div>
    </nav>
  );
};

export default NavbarWrite;
