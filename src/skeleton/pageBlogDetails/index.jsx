import { SkeletonBlogCardList } from "../../components/blog/loading";

export default function SkeletonBlogDetails() {
  return (
    <>
      <section className="about__blog">
        <div className="container mx-auto xl:w-[680px] xl:my-20">
          <div className="">
            <h3 className="w-[400px] h-[30px] rounded-full animate-pulse bg-gray-300"></h3>
          </div>

          <div className="account flex justify-start items-center gap-3 my-10">
            <div className="">
              <div className="w-[44px] h-[44px] rounded-full animate-pulse bg-gray-300"></div>
            </div>

            <div className="flex flex-col gap-2">
              <div className="flex justify-start items-center gap-2">
                <h3 className="w-[50px] h-[20px] rounded-full animate-pulse bg-gray-300"></h3>
                <div className="w-[20px] h-[20px] rounded-full animate-pulse bg-gray-300"></div>
                <button className="w-[50px] h-[20px] rounded-full animate-pulse bg-gray-300"></button>
              </div>

              <div className="flex justify-start items-center gap-2">
                <div className="w-[80px] h-[20px] rounded-full animate-pulse bg-gray-300"></div>

                <div className="w-[20px] h-[20px] rounded-full animate-pulse bg-gray-300"></div>

                <div className="w-[50px] h-[20px] rounded-full animate-pulse bg-gray-300"></div>
              </div>
            </div>
          </div>

          <div className="mt-6 py-3 border-t-[1px] border-t-gray-200 border-b-[1px] border-b-gray-200 flex justify-between items-center">
            <div className=" flex justify-start items-center gap-3">
              <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
              <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
            </div>
            <div className="flex justify-start items-center gap-3">
              <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
              <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
            </div>
          </div>

          <div className="content__blog my-6">
            <div className="grid grid-cols-3 gap-4">
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
              <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
            </div>
          </div>

          <div className="flex justify-start items-center gap-5">
            <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
            <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
            <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
            <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
          </div>

          <div className="mt-6 py-3 border-t-[1px] border-t-gray-200 border-b-[1px] border-b-gray-200 flex justify-between items-center">
            <div className=" flex justify-start items-center gap-3">
              <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
              <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
            </div>
            <div className="flex justify-start items-center gap-3">
              <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
              <div className="w-[50px] h-[30px] rounded-full animate-pulse bg-gray-300"></div>
            </div>
          </div>
        </div>
      </section>

      <section className="about__author">
        <div className="py-16 bg-zinc-50">
          <div className="container w-[680px] mx-auto">
            <div className="about__account">
              <div className="">
                <div className="w-[72px] h-[72px] rounded-full animate-pulse bg-gray-300"></div>
              </div>

              <div className="my-5 border-b-[1px] border-b-zinc-100">
                <div className="flex justify-between items-center">
                  <div className="write__by">
                    <h5 className="font-semibold text-2xl w-[60px] h-[30px] rounded-xl animate-pulse bg-gray-300"></h5>
                  </div>

                  <div className="action__account flex items-center gap-3">
                    <button className="btn__follow h-10 rounded-3xl leading-5 px-3 bg-green-700 text-white text-sm hover:bg-black transition-all">
                      Theo dõi
                    </button>
                    <button className="btn__follow h-10 rounded-3xl leading-5 px-3 bg-green-700 text-white text-sm hover:bg-black transition-all">
                      <svg
                        className="w-4 h-4 text-white dark:text-white "
                        aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 20 21"
                      >
                        <path
                          stroke="currentColor"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                          d="M10 3.464V1.1m0 2.365a5.338 5.338 0 0 1 5.133 5.368v1.8c0 2.386 1.867 2.982 1.867 4.175C17 15.4 17 16 16.462 16H3.538C3 16 3 15.4 3 14.807c0-1.193 1.867-1.789 1.867-4.175v-1.8A5.338 5.338 0 0 1 10 3.464ZM1.866 8.832a8.458 8.458 0 0 1 2.252-5.714m14.016 5.714a8.458 8.458 0 0 0-2.252-5.714M6.54 16a3.48 3.48 0 0 0 6.92 0H6.54Z"
                        ></path>
                      </svg>
                    </button>
                  </div>
                </div>

                <div className="follower mb-3 w-[60px] h-[30px] rounded-xl animate-pulse bg-gray-300">
                  <span className="text-sm"></span>
                </div>

                <div className="introduce">
                  <div className="content mt-3">
                    <div className="grid grid-cols-3 gap-4">
                      <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
                      <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
                      <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
                      <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
                      <div className="h-2 bg-gray-300 animate-pulse rounded col-span-2"></div>
                      <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
                      <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
                      <div className="h-2 bg-gray-300 animate-pulse rounded col-span-1"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="about__blogList py-7 border-t-zinc-300 border-t-[1px] border-b-zinc-300 border-b-[1px]">
              <div className="about__blogList-title mb-5">
                <h5 className="w-[100px] h-[30px] rounded-xl animate-pulse bg-gray-300"></h5>
              </div>

              <div className="blog__list grid grid-cols-2 gap-3">
                <SkeletonBlogCardList />
              </div>
            </div>

            <div className="mt-6">
              <button className="w-[100px] h-[30px] rounded-xl animate-pulse bg-gray-300"></button>
            </div>
          </div>
        </div>
      </section>

      <section className="blog__recommentborder-t-zinc-300 border-t-[1px] border-b-zinc-300 border-b-[1px]">
        <div className="py-16 bg-zinc-50">
          <div className="container w-[680px] mx-auto">
            <div className="about__blogList py-7 ">
              <div className="about__blogList-title mb-5">
                <h5 className="text-lg font-medium w-[100px] h-[30px] rounded-xl animate-pulse bg-gray-300"></h5>
              </div>

              <div className="blog__list grid grid-cols-2 gap-3">
                {/* <Each of={blogs} render={(blog, index) => <BlogCardItem blog={blog} key={index} />} /> */}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
