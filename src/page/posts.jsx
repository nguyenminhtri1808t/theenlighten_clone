import React, { Suspense, useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { publicPostsApi } from "../apis/public/postsApi";

import BlogCardItem from "../components/blog/blogCard";
import { Each } from "../utils/each";
import {
  SkeletonBlogCardList,
  SkeletonBlogDoubleCardList,
} from "../components/blog/loading";
import SkeletonPageBlogsOrBlogTopicAll from "../skeleton/blogTopicOrBlogAll";

BlogExplorePage.propTypes = {};

function BlogExplorePage(props) {
  const [isLoading, setLoading] = useState(false);
  const [firstLoading, setFirstLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [errors, setErrors] = useState(null);
  const [blogs, setBlogs] = useState([]);
  const observerTarget = useRef(null);

  const fetchBlogs = async () => {
    setLoading(true);
    setErrors(null);
    try {
      const res = await publicPostsApi.gelAllPosts(page);
      const newBlogs = res.payload;
      setBlogs((prev) => [...prev, ...newBlogs]);
      setFirstLoading(false);
    } catch (error) {
      setErrors(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchBlogs(page);
  }, [page]);

  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting) {
          setPage((prev) => prev + 1);
        }
      },
      { threshold: 1 }
    );

    if (observerTarget.current) {
      observer.observe(observerTarget.current);
    }

    return () => {
      if (observerTarget.current) {
        observer.unobserve(observerTarget.current);
      }
    };
  }, []);
  const doubleBlogTopic = blogs.slice(0, 2);
  const moreBlogTopic = blogs.slice(2);

  // if (isLoading === true) {
  //   return (
  //     <>
  //       <SkeletonPageBlogsOrBlogTopicAll />
  //     </>
  //   );
  // }

  return (
    <div className="container mx-auto topic_about lg:w-[1200px]">
      <div className="text-center py-3 capitalize md:mt-10 font-bold text-2xl md:text-3xl lg:text-4xl md:mb-16">
        {/* {formatSlug} */}
        Blog Explore
      </div>

      <div className="mb-10">
        <div className="grid grid-cols-1 gap-3 md:grid-cols-2 md:gap-x-5 md:gap-y-10 lg:grid-cols-2 lg:gap-10">
          {firstLoading ? (
            <SkeletonBlogDoubleCardList />
          ) : (
            <Each
              of={doubleBlogTopic}
              render={(item, index) => (
                <BlogCardItem
                  key={index}
                  blog={item}
                  accountName={item.creator.nickname}
                  accountAvatar={item.creator.avatarUrl}
                />
              )}
            />
          )}
        </div>
        <div className="pt-10 grid grid-cols-1 gap-3 md:grid-cols-2 md:gap-x-5 md:gap-y-10 lg:grid-cols-3 lg:gap-10">
          {firstLoading ? (
            <SkeletonBlogCardList />
          ) : (
            <Each
              of={moreBlogTopic}
              render={(item, index) => (
                <BlogCardItem
                  key={index}
                  blog={item}
                  accountName={item.creator.nickname}
                  accountAvatar={item.creator.avatarUrl}
                />
              )}
            />
          )}
          {isLoading && <div>Loading...</div>}
          {errors && <p>Error: {errors.message}</p>}
          <div ref={observerTarget}></div>
        </div>
      </div>
    </div>
  );
}

export default BlogExplorePage;
