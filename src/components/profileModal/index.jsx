import { yupResolver } from "@hookform/resolvers/yup";
import React, { useState } from "react";
import { set, useForm } from "react-hook-form";
import * as yup from "yup";
import { imageService } from "../../apis/private/imageApi";
import { ProfileApi } from "../../apis/profileApi";
import { useDispatch } from "react-redux";
import { authenticationActions } from "../../slice/authenticationSlice";
import toast from "react-hot-toast";

export const AvatarDefault =
  "https://res.cloudinary.com/dbxc9n6e4/image/upload/v1705042274/ruznypbsucdskzvqk4m0.jpg";

const schema = yup.object({
  nickname: yup.string().required().min(4).max(50),
  // bio: yup.string().min(50).max(100),
});

const ProfileModal = (props) => {
  const { image, nickname, handleModalProfile } = props;
  const [avatar, setAvatar] = useState(image);
  const [fileAvatar, setFileAvatar] = useState(null);
  const [deleteAvatar, setDeleteAvatar] = useState(false);
  const [confirmClose, setConfirmClose] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      nickname: nickname,
    },
  });

  const handleAvatar = (event) => {
    if (event.target.files[0]) {
      setFileAvatar(event.target.files[0]);
      setAvatar(URL.createObjectURL(event.target.files[0]));
    }
  };

  const handleAvatarDefault = () => {
    setAvatar(AvatarDefault);
  };

  const hideModalProfile = () => {
    console.log(avatar, image);
    if (watch("nickname") !== nickname || avatar !== image) {
      setConfirmClose(true);
    } else {
      handleModalProfile();
    }
  };

  const handleConfirmClose = (value) => {
    if (value) {
      handleModalProfile();
      setConfirmClose(false);
      reset();
      setAvatar(image);
    } else {
      setConfirmClose(false);
    }
  };

  const onSubmit = async (data) => {
    let newImageAvatar;
    if (avatar.startsWith("blob")) {
      toast.success("Đang cập nhật ảnh đại diện...");
      setIsLoading(true);
      await imageService
        .postImages(fileAvatar)
        .then((response) => {
          newImageAvatar = response.payload;
          toast.success("Cập nhật ảnh đại diện thành công !");
        })
        .catch((err) => {
          // if (err.response.status === 413) {
          //   toast.error(
          //     "Ảnh đại diện bạn chọn có kích thước lớn hơn cho phép !"
          //   );
          // } else {
          //   toast.error("Cập nhật ảnh đại diện thất bại !");
          // }
          console.log("error : ", err);
        })
        .finally(() => {
          setIsLoading(false);
        });
    } else {
      newImageAvatar = avatar;
    }
    updateProfile({ nickname: data.nickname, avatarUrl: newImageAvatar });
  };

  const updateProfile = async (data) => {
    await ProfileApi.updateProfile(data)
      .then((response) => {
        dispatch(authenticationActions.updateUser(response.payload));
        handleModalProfile();
        setAvatar(data.avatarUrl);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="bg-white shadow-md p-8 max-w-[600px] rounded-lg">
      <div className="relative">
        <p className="font-bold text-2xl">Thông tin cá nhân</p>
        <div className="mt-8">
          <p className="mb-2">Ảnh đại diện</p>
          <div className="flex my-4">
            <img className="w-20 h-20 rounded-full" src={avatar} alt="" />
            <div className="ml-4">
              <label
                htmlFor="imageAvatar"
                className="text-green-800 px-4 py-2 cursor-pointer"
              >
                Cập nhật
              </label>
              <input
                type="file"
                name=""
                className="hidden"
                id="imageAvatar"
                onChange={(event) => handleAvatar(event)}
              />
              <button
                onClick={handleAvatarDefault}
                className="text-red-500 px-4 py-2 cursor-pointer"
              >
                Xóa
              </button>
              <p className="text-sm px-4 text-gray-500">
                Khuyến nghị: JPG, PNG hoặc GIF hình vuông, ít nhất 1.000 pixel
                mỗi cạnh.
              </p>
            </div>
          </div>
          <p className="">Tên *</p>
          <div>
            <div>
              <input
                type="text"
                id="first_name"
                {...register("nickname")}
                className="bg-transparent font-medium border-b border-gray-300 text-gray-900 text-sm focus:ring-0 focus:outline-none block w-full p-2.5 pl-0"
                required
              />
              <div className="flex">
                <p className="text-sm mt-1 text-gray-600">
                  Xuất hiện trên trang Hồ sơ của bạn, dưới dạng dòng tên và
                  trong câu trả lời của bạn.
                </p>
                <p className="text-sm mt-1">{watch("nickname").length}/100</p>
              </div>
            </div>
          </div>
          <div className="mt-4 text-end space-x-3">
            <button
              onClick={hideModalProfile}
              className="px-5 py-2.5 rounded-full border border-green-700 text-green-700"
            >
              Hủy bỏ
            </button>
            <button
              onClick={handleSubmit(onSubmit)}
              disabled={isLoading}
              className={`px-5 py-2.5 rounded-full bg-green-700 text-white ${
                isLoading && "opacity-50"
              }`}
            >
              Lưu
            </button>
          </div>
        </div>
        <div className="absolute top-0 right-0">
          <button onClick={hideModalProfile}>
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M18 6L6 18"
                stroke="#33363F"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M6 6L18 18"
                stroke="#33363F"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </button>
        </div>
      </div>
      <div
        className={`${
          confirmClose ? "" : "hidden"
        } fixed shadow-xl inset-0 bg-white/60 flex justify-center items-center`}
      >
        <div className="relative p-4 w-full max-w-md h-full md:h-auto">
          <div className="relative p-4 text-center bg-white rounded-lg shadow 0 sm:p-5">
            <button
              onClick={() => handleConfirmClose(false)}
              type="button"
              className="text-gray-400 absolute top-2.5 right-2.5 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center "
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
            <svg
              className="text-gray-400  w-11 h-11 mb-3.5 mx-auto"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                clipRule="evenodd"
              ></path>
            </svg>
            <p className="mb-4 text-gray-500 ">
              "Bạn có chắc chắn muốn hủy bỏ? Các thay đổi của bạn sẽ không được
              lưu."
            </p>
            <div className="flex justify-center items-center space-x-4">
              <button
                onClick={() => handleConfirmClose(false)}
                type="button"
                className="py-2 px-3 text-sm font-medium text-gray-500 bg-white rounded-lg border border-gray-200 hover:bg-gray-100 focus:ring-0 focus:outline-none hover:text-gray-900 focus:z-10"
              >
                Thoát
              </button>
              <button
                onClick={() => handleConfirmClose(true)}
                type="submit"
                className="py-2 px-3 text-sm font-medium text-center text-white bg-red-600 rounded-lg hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 "
              >
                Hủy bỏ
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileModal;
