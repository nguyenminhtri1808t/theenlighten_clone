import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { publicPostsApi } from "../apis/public/postsApi";
import { Link, useParams } from "react-router-dom";
import {
  SkeletonBlogCardList,
  SkeletonBlogDoubleCardList,
} from "../components/blog/loading";
import { Each } from "../utils/each";
import BlogCardItem from "../components/blog/blogCard";
import { EmptyDataComponent } from "../components/emptyData";

TopicPage.propTypes = {};


function TopicPage() {
  const { topic } = useParams();
  const [isLoading, setLoading] = useState(false);
  const [firstLoading, setFirstLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [errors, setErrors] = useState(null);
  const [blogs, setBlogs] = useState([]);
  const observerTarget = useRef(null);

  const formatSlug = topic.replace(/-/g, " ");

  const fetchBlogs = async () => {
    setLoading(true);
    setErrors(null);
    try {
      const res = await publicPostsApi.getPostsByTopic(formatSlug, page);
      const newBlogs = res.payload;
      setBlogs((prev) => [...prev, ...newBlogs]);
      setFirstLoading(false);
    } catch (error) {
      setErrors(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchBlogs(page);
  }, [page]);

  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting) {
          setPage((prev) => prev + 1);
        }
      },
      { threshold: 1 }
    );

    if (observerTarget.current) {
      observer.observe(observerTarget.current);
    }

    return () => {
      if (observerTarget.current) {
        observer.unobserve(observerTarget.current);
      }
    };
  }, []);

  const doubleBlogTopic = blogs.slice(0, 2);
  const moreBlogTopic = blogs.slice(2);

  if(firstLoading) return (<div>Loading...</div>)

  return (
    <>
      {/* <div className="container mx-auto lg:w-[1200px] overflow-hidden">
        <TopicListVerticalComponent />
      </div> */}

      <div className="container mx-auto topic_about lg:w-[1200px]">
        <div className="text-center py-3 capitalize md:mt-6 font-bold text-2xl md:text-3xl lg:text-4xl">
          {formatSlug}
        </div>
        <div className="flex justify-center items-center gap-1 text-xs text-gray-600 font-medium mb-5 md:text-base">
          <div className="">Topic</div>
          <div className="">-</div>
          <div className="">3.3M Người theo dõi</div>
          <div className="">-</div>
          <div className="">2.1k Bài viết</div>
        </div>

        <div className="text-center md:mb-10">
          <button className="btn-follow py-2 px-4 bg-black text-white rounded-full">
            Theo dõi
          </button>
        </div>

        {blogs.length === 0 ? (
          <EmptyDataComponent />
        ) : (
          <>
            <div className="mb-10 mt-28">
              <div className="grid grid-cols-1 gap-3 md:grid-cols-2 md:gap-x-5 md:gap-y-10 lg:grid-cols-2 lg:gap-10">
                {firstLoading ? (
                  <SkeletonBlogDoubleCardList />
                ) : (
                  <Each
                    of={doubleBlogTopic}
                    render={(item, index) => (
                      <Link to={`/blogs/${item.id}`} key={index}>
                        <BlogCardItem
                          key={index}
                          blog={item}
                          accountName={item.creator.nickname}
                          accountAvatar={item.creator.avatarUrl}
                        />
                      </Link>
                    )}
                  />
                )}
              </div>
              <div className="pt-10 grid grid-cols-1 gap-3 md:grid-cols-2 md:gap-x-5 md:gap-y-10 lg:grid-cols-3 lg:gap-10">
                {firstLoading ? (
                  <SkeletonBlogCardList />
                ) : (
                  <Each
                    of={moreBlogTopic}
                    render={(item, index) => (
                      <Link to={`/blogs/${item.id}`} key={index}>
                        <BlogCardItem blog={item} />
                      </Link>
                    )}
                  />
                )}
                {isLoading && <div>Loading...</div>}
                {errors && <p>Error: {errors.message}</p>}
                <div ref={observerTarget}></div>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
}

export default TopicPage;
