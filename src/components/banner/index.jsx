import TextRandom from "../textrandom";

export default function BannerComponent() {
  return (
    <>
      <section className="py-3 bg-amber-400 h-[500px] pl-14">
        <div className="container mx-auto flex items-center justify-between h-full overflow-hidden">
          <div className="md:w-[60%] xl:w-[50%]">
            <h2 className="banner text font-semibold py-2 text-8xl w-[60%] md:w-full  xl:w-[100%]">
              Chia sẻ câu chuyện của bạn
            </h2>
            <p className="my-7 md:text-2xl banner text">
              Khám phá những câu chuyện, suy nghĩ và kiến ​​thức chuyên môn của
              các tác giả về bất kỳ chủ đề nào.
            </p>
            <button className="py-2.5 px-6 rounded-full border-black border text-black font-roboto text-sm uppercase md:text-lg font-medium hover:bg-black hover:text-white transition-all duration-300 ease-linear">
              Bắt đầu
            </button>
          </div>
          <div className="relative translate-x-1/4 ">
            <TextRandom />
          </div>
        </div>
      </section>
    </>
  );
}
